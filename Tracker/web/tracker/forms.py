from datetime import datetime
from dateutil.relativedelta import relativedelta

from django import forms

from . import get_api
from tr_library.models.task import (
    Priority,
    Status
)

DATE_FORMAT = '%Y-%m-%d %H:%M'


class TaskForm(forms.Form):
    name = forms.CharField(max_length=30)
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        initial=format(datetime.now(), DATE_FORMAT)
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        required=False
    )
    priority = forms.ChoiceField(
        choices=[(priority.value, priority.name) for priority in Priority]
    )
    folder_id = forms.ChoiceField()

    def __init__(self, user, task_id, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        api = get_api()

        folders = api.get_folders(user)
        folder_list = [(folder.id, "ID: %d, name: %s "
                        % (folder.id, folder.name)) for folder in folders]
        folder_list.insert(0, (0, "without"))

        task = None
        if task_id is not None:
            task = api.get_task(user, task_id)
            folder_list.insert(0, (-1, "do not change"))

        self.fields['folder_id'] = forms.ChoiceField(
            label='Folder',
            choices=folder_list,
        )

        self.fields['start_time'].initial = format(datetime.now(),
                                                   DATE_FORMAT)


class FolderForm(forms.Form):
    name = forms.CharField(max_length=50)


class TagForm(forms.Form):
    name = forms.CharField(max_length=20)


class NotificationForm(forms.Form):
    date = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        initial=format(datetime.now(), DATE_FORMAT)
    )

    def __init__(self, *args, **kwargs):
        super(NotificationForm, self).__init__(*args, **kwargs)

        self.fields['date'].initial = format(
            datetime.now() + relativedelta(minutes=1),
            DATE_FORMAT
        )


class ExecutorForm(forms.Form):
    username = forms.CharField(max_length=50)


class TaskPlanForm(forms.Form):
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        initial=format(datetime.now(), DATE_FORMAT)
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        required=False
    )
    interval = forms.CharField(
        max_length=20,
        help_text="Enter a value, then one of the keys: second, minute, hour, day, week, month or year."
    )
    total_tasks = forms.CharField(max_length=10, required=False)
    task_id = forms.ChoiceField()

    def __init__(self, user, *args, **kwargs):
        super(TaskPlanForm, self).__init__(*args, **kwargs)
        api = get_api()

        tasks = api.get_tasks(user)
        tasks_list = [(task.id, "ID: %d, name: %s "
                       % (task.id, task.name)) for task in tasks]

        self.fields['task_id'] = forms.ChoiceField(
            label='Task',
            choices=tasks_list,
        )

        self.fields['start_time'].initial = format(
            datetime.now() + relativedelta(minutes=1),
            DATE_FORMAT
        )


class SubtaskForm(forms.Form):
    subtask_id = forms.ChoiceField()

    def __init__(self, user, *args, **kwargs):
        super(SubtaskForm, self).__init__(*args, **kwargs)
        api = get_api()

        tasks = api.get_tasks(user, status=Status.TODO)
        tasks.extend(api.get_tasks(user, status=Status.IN_PROGRESS))

        subtask_list = ([(task.id, "ID: %d name: %s"
                          % (task.id, task.name)) for task in tasks])

        self.fields['subtask_id'] = forms.ChoiceField(
            choices=subtask_list,
            label='Subtask'
        )


class SearchForm(forms.Form):
    name = forms.CharField(max_length=30, required=False)
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        required=False
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(
            attrs={'type': 'datetime'},
            format=DATE_FORMAT
        ),
        required=False
    )
    priority = forms.ChoiceField(required=False)
    status = forms.ChoiceField(required=False)
    parent_task_id = forms.ChoiceField(required=False)
    executor = forms.BooleanField(required=False)

    def __init__(self, user, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        api = get_api()

        priority_list = [(priority.value, priority.name) for priority in Priority]
        priority_list.insert(0, (0, "ALL"))

        self.fields['priority'] = forms.ChoiceField(
            label='Priority',
            choices=priority_list,
        )

        status_list = [(status.value, status.name) for status in Status]
        status_list.insert(0, (0, "ALL"))

        self.fields['status'] = forms.ChoiceField(
            label='Status',
            choices=status_list,
        )

        tasks = api.get_tasks(user)
        task_list = [(task.id, "ID: %d, name: %s "
                      % (task.id, task.name)) for task in tasks]
        task_list.insert(0, (0, "ALL"))

        self.fields['parent_task_id'] = forms.ChoiceField(
            label='Parent task',
            choices=task_list,
        )
