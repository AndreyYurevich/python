from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views

task_patterns = [
    url(r'^complete/$', views.complete_task, name='task_complete'),
    url(r'^delete/$', views.delete_task, name='task_delete'),
    url(r'^edit/$', views.edit_task, name='task_edit'),
    url(r'^show/$', views.show_task, name='task_show'),

    url(r'^notification/add/$', views.notification_add, name='notification_add'),
    url(r'^notification/(?P<notification_id>\d+)/delete/$', views.notification_delete, name='notification_delete'),

    url(r'^executor/add/$', views.add_executor, name='executor_add'),
    url(r'^executor/(?P<executor_user>[A-Za-z0-9]+)/delete/$', views.executor_delete, name='executor_delete'),

    url(r'^tag/add/$', views.add_tag, name='tag_add'),
    url(r'^tag/(?P<tag_id>\d+)/delete/$', views.tag_delete, name='tag_delete'),

    url(r'^attach/$', views.attach_subtask, name='attach_subtask'),
    url(r'^subtask/(?P<subtask_id>\d+)/detach/$', views.detach_subtask, name='detach_subtask'),
]

urlpatterns = [
    url(r'^$', views.password_edit, name='home'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'home'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),

    url(r'^folder/(?P<folder>[a-z0-9]+)/task/list/$', views.task_list, name='task_list'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/add/$', views.task_add, name='task_add'),

    url(r'^folder/(?P<folder_id>[0-9]+)/delete/$', views.delete_folder, name='folder_delete'),
    url(r'^folder/(?P<folder_id>[0-9]+)/edit/$', views.edit_folder, name='folder_edit'),
    url(r'^folder/list/$', views.folder_list, name='folder_list'),
    url(r'^folder/add/$', views.folder_add, name='folder_add'),

    url(r'^tag/(?P<tag_id>[0-9]+)/show/$', views.show_tag, name='tag_show'),

    url(r'^search/$', views.search, name='search'),

    url(r'^plan/list/$', views.task_plan_list, name='plan_list'),
    url(r'^plan/add/$', views.add_task_plan, name='plan_add'),
    url(r'^plan/(?P<task_plan_id>\d+)/delete/$', views.delete_task_plan, name='plan_delete'),
    url(r'^plan/(?P<task_plan_id>\d+)/show/$', views.show_task_plan, name='plan_show'),

    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/', include(task_patterns)),
]
