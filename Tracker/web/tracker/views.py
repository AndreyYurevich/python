from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import (
    UserCreationForm,
    PasswordChangeForm
)
from django.contrib.auth.models import User
from django.shortcuts import (
    render,
    redirect
)

from . import get_api
from tr_library.models.task import (
    Status,
    Priority
)
from tr_library.api.exceptions import (
    LibraryError,
    TimeError,
    AttachError,
    DetachError
)
from .forms import (
    TaskForm,
    FolderForm,
    TagForm,
    NotificationForm,
    ExecutorForm,
    TaskPlanForm,
    SubtaskForm,
    SearchForm
)


def generate_task_plan(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            api = get_api()
            user = request.user.username

            api.generate_a_task_plan(user)

        return func(request, *args, **kwargs)

    return wrapper


def show_notifications(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            api = get_api()
            user = request.user.username

            notifications = api.show_notifications(user)

            for notification in notifications:
                task = notification.task
                message = ("Do not forget about the task 'ID: %d, name: %s'"
                           % (task.id, task.name))

                messages.success(request, message)

        return func(request, *args, **kwargs)

    return wrapper


@generate_task_plan
@show_notifications
def home(request):
    return render(request, 'tracker/home.html')


@generate_task_plan
@show_notifications
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})


@generate_task_plan
@show_notifications
def task_list(request, folder):
    api = get_api()
    user = request.user.username

    folders = api.get_folders(user)

    if folder == 'without':
        tasks = api.get_tasks(
            user=user,
            status=Status.TODO,
            without_folder=True,
            parent_task=True
        )
        tasks.extend(api.get_tasks(
            user=user,
            status=Status.IN_PROGRESS,
            without_folder=True,
            parent_task=True)
        )
    elif folder == 'archive':
        tasks = api.get_tasks(
            user=user,
            status=Status.FAILED,
            parent_task=True
        )
        tasks.extend(api.get_tasks(
            user=user,
            status=Status.DONE,
            parent_task=True)
        )
    elif folder == 'executable':
        tasks = api.get_tasks(user=user, executor=True, parent_task=True)
    elif folder == 'template':
        tasks = api.get_task_templates(user)
    else:
        folder_id = int(folder)

        tasks = api.get_tasks(
            user=user,
            folder_id=folder_id,
            status=Status.TODO,
            parent_task=True
        )
        tasks.extend(api.get_tasks(
            user=user,
            folder_id=folder_id,
            status=Status.IN_PROGRESS,
            parent_task=True)
        )

    return render(request, 'tracker/task_list.html',
                  {'tasks': tasks, 'folder': folder, 'folders': folders})


@generate_task_plan
@show_notifications
def task_add(request, folder):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = TaskForm(user, None, request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            start_time = form.cleaned_data['start_time']
            end_time = form.cleaned_data['end_time']
            priority = int(form.cleaned_data['priority'])
            folder_id = int(form.cleaned_data['folder_id'])

            priority = Priority(priority).name

            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                task_id = api.create_task(
                    user=user,
                    name=name,
                    start_time=start_time,
                    end_time=end_time,
                    priority=priority
                )

                if folder_id != 0:
                    api.add_task_to_folder(user, task_id, folder_id)
            except TimeError as e:
                form.add_error('start_time', str(e))
                return render(request, 'tracker/task_add.html', {'form': form})

            return redirect('task_list', folder)
    else:
        form = TaskForm(
            user,
            None,
            initial={'folder_id': 0 if folder == "without" else folder}
        )

    return render(request, 'tracker/task_add.html', {'form': form})


@generate_task_plan
@show_notifications
def show_task(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if folder == 'template':
        task = api.get_task_template(user, task_id)
    else:
        task = api.get_task(user, task_id)

    return render(request, 'tracker/task_show.html',
                  {'folder': folder, 'task': task})


@generate_task_plan
@show_notifications
def edit_task(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = TaskForm(user, task_id, request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            start_time = form.cleaned_data['start_time']
            end_time = form.cleaned_data['end_time']
            priority = int(form.cleaned_data['priority'])
            folder_id = int(form.cleaned_data['folder_id'])

            priority = Priority(priority).name

            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                api.edit_task(
                    user=user,
                    task_id=task_id,
                    name=name,
                    start_time=start_time,
                    end_time=end_time,
                    priority=priority
                )

                if folder_id == 0:
                    folder = 'without'
                    api.delete_task_from_folder(user, task_id)
                elif folder_id > 0:
                    folder = folder_id
                    api.add_task_to_folder(user, task_id, folder_id)
            except TimeError as e:
                form.add_error('end_time', str(e))
                return render(request, 'tracker/task_edit.html', {'form': form})
            except (AttachError, DetachError) as e:
                form.add_error('folder_id', str(e))
                return render(request, 'tracker/task_edit.html', {'form': form})
            except LibraryError as e:
                form.add_error(None, str(e))
                return render(request, 'tracker/task_edit.html', {'form': form})

            return redirect('task_list', folder)

    else:
        task = api.get_task(user, task_id)
        print(task.priority)
        form = TaskForm(
            user, task_id,
            initial={
                'name': task.name,
                'start_time': task.start_time,
                'end_time': task.end_time,
                'priority': task.priority.value,
            }
        )

    return render(request, 'tracker/task_edit.html', {'form': form})


@generate_task_plan
@show_notifications
def complete_task(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        api.complete_task(user, task_id)

    return redirect('task_list', folder)


@generate_task_plan
@show_notifications
def delete_task(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        api.delete_task(user, task_id)

    return redirect('task_list', folder)


@generate_task_plan
@show_notifications
def folder_add(request):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = FolderForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']

            api.create_folder(user, name)

            return redirect('folder_list')
    else:
        form = FolderForm()

    return render(request, 'tracker/folder_add.html', {'form': form})


@generate_task_plan
@show_notifications
def edit_folder(request, folder_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = FolderForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                api.edit_folder(
                    user=user,
                    folder_id=folder_id,
                    name=name
                )
            except ValueError as e:
                form.add_error('name', str(e))
                return render(request, 'tracker/folder_edit.html',
                              {'form': form})

            return redirect('folder_list')

    else:
        folder = api.get_folder(user, folder_id)
        form = FolderForm(
            initial={'name': folder.name}
        )

    return render(request, 'tracker/folder_edit.html', {'form': form})


@generate_task_plan
@show_notifications
def delete_folder(request, folder_id):
    api = get_api()
    user = request.user.username

    api.delete_folder(user, folder_id)

    return redirect('folder_list')


@generate_task_plan
@show_notifications
def add_tag(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = TagForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                api.add_tag(user, task_id, name)
            except LibraryError as e:
                form.add_error('name', str(e))
                return render(request, 'tracker/tag_add.html', {'form': form})

            return redirect('task_show', folder=folder, task_id=task_id)
    else:
        form = TagForm()

    return render(request, 'tracker/tag_add.html', {'form': form})


@generate_task_plan
@show_notifications
def show_tag(request, tag_id):
    api = get_api()
    user = request.user.username

    tag = api.get_tag(user, tag_id)

    tasks = tag.tasks
    folders = []
    for task in tasks:
        if task.folder is not None:
            folders.append(task.folder.id)
        elif task.status == Status.DONE or task.status == Status.FAILED:
            folders.append('archive')
        else:
            folders.append('without')
    folder_task_list = list(zip(folders, tag.tasks))

    return render(request, 'tracker/tag_show.html',
                  {'tag': tag, 'folder_task_list': folder_task_list})


def tag_delete(request, folder, task_id, tag_id):
    api = get_api()
    user = request.user.username

    api.delete_tag(user, tag_id, task_id)

    return redirect('task_show', folder=folder, task_id=task_id)


@generate_task_plan
@show_notifications
def notification_add(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = NotificationForm(request.POST)

        if form.is_valid():
            date = form.cleaned_data['date']

            if date is not None:
                date = date.replace(tzinfo=None)

            try:
                api.add_notification(user, date, task_id)
            except TimeError as e:
                form.add_error('date', str(e))
                return render(request, 'tracker/notification_add.html',
                              {'form': form})

            return redirect('task_show', folder=folder, task_id=task_id)
    else:
        form = NotificationForm()

    return render(request, 'tracker/notification_add.html', {'form': form})


@generate_task_plan
@show_notifications
def notification_delete(request, folder, task_id, notification_id):
    api = get_api()
    user = request.user.username

    api.delete_notification(user, notification_id)

    return redirect('task_show', folder=folder, task_id=task_id)


@generate_task_plan
@show_notifications
def add_executor(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = ExecutorForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']

            try:
                User.objects.get(username=username)
                api.add_executor_to_task(user, task_id, username)
            except User.DoesNotExist:
                form.add_error('username', 'User is not registered.')
                return render(request, 'tracker/executor_add.html',
                              {'form': form})
            except LibraryError as e:
                form.add_error('username', str(e))
                return render(request, 'tracker/executor_add.html',
                              {'form': form})

            return redirect('task_show', folder=folder, task_id=task_id)

    else:
        form = ExecutorForm()

    return render(request, 'tracker/executor_add.html', {'form': form})


@generate_task_plan
@show_notifications
def executor_delete(request, folder, task_id, executor_user):
    api = get_api()
    user = request.user.username

    api.delete_executor_from_task(user, task_id, executor_user)

    return redirect('task_show', folder=folder, task_id=task_id)


@generate_task_plan
@show_notifications
def task_plan_list(request):
    api = get_api()
    user = request.user.username

    task_plans = api.get_task_plans(user)
    for task_plan in task_plans:
        task_plan.interval = (str(task_plan.interval).split("(")[1]
                              .replace(")", ""))

    return render(request, 'tracker/plan_list.html', {'task_plans': task_plans})


@generate_task_plan
@show_notifications
def add_task_plan(request):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = TaskPlanForm(user, request.POST)

        if form.is_valid():
            start_time = form.cleaned_data['start_time']
            end_time = form.cleaned_data['end_time']
            interval = form.cleaned_data['interval']
            total_tasks = form.cleaned_data['total_tasks']
            task_id = int(form.cleaned_data['task_id'])

            if total_tasks != '':
                total_tasks = int(total_tasks)
            else:
                total_tasks = None
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                plan = api.create_task_plan(
                    user=user,
                    interval=interval,
                    task_id=task_id,
                    start_time=start_time,
                    end_time=end_time,
                    total_tasks_count=total_tasks
                )

            except TimeError as e:
                form.add_error('start_time', str(e))
                return render(request, 'tracker/plan_add.html', {'form': form})

            return redirect('plan_list')
    else:
        form = TaskPlanForm(user, None)

    return render(request, 'tracker/plan_add.html', {'form': form})


@generate_task_plan
@show_notifications
def show_task_plan(request, task_plan_id):
    api = get_api()
    user = request.user.username

    task_plan = api.get_task_plan(user, task_plan_id)

    tasks = task_plan.tasks
    folders = []
    for task in tasks:
        if task.folder is not None:
            folders.append(task.folder.id)
        elif task.status == Status.DONE or task.status == Status.FAILED:
            folders.append('archive')
        else:
            folders.append('without')
    folder_plan_list = list(zip(folders, task_plan.tasks))

    return render(request, 'tracker/plan_show.html',
                  {'task_plan': task_plan, 'folder_plan_list': folder_plan_list})


@generate_task_plan
@show_notifications
def delete_task_plan(request, task_plan_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        api.delete_task_plan(user, task_plan_id)

    return redirect('plan_list')


@generate_task_plan
@show_notifications
def folder_list(request):
    api = get_api()
    user = request.user.username

    folders = api.get_folders(user)

    return render(request, 'tracker/folder_list.html', {'folders': folders})


@generate_task_plan
@show_notifications
def attach_subtask(request, folder, task_id):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = SubtaskForm(user, request.POST)

        if form.is_valid():
            suntask_id = form.cleaned_data['subtask_id']

            try:
                api.attach_subtask(user, task_id, suntask_id)

            except LibraryError as e:
                form.add_error('subtask_id', str(e))
                return render(request, 'tracker/task_attach.html', {'form': form})

            return redirect('task_show', folder=folder, task_id=task_id)
    else:
        form = SubtaskForm(user)

    return render(request, 'tracker/task_attach.html', {'form': form})


@generate_task_plan
@show_notifications
def detach_subtask(request, folder, task_id, subtask_id):
    api = get_api()
    user = request.user.username

    api.detach_subtask(user, subtask_id)

    return redirect('task_show', folder=folder, task_id=task_id)


@generate_task_plan
@show_notifications
def search(request):
    api = get_api()
    user = request.user.username

    if request.method == 'POST':
        form = SearchForm(user, request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            start_time = form.cleaned_data['start_time']
            end_time = form.cleaned_data['end_time']
            priority = int(form.cleaned_data['priority'])
            status = int(form.cleaned_data['status'])
            parent_task_id = int(form.cleaned_data['parent_task_id'])
            executor = form.cleaned_data['executor']

            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)
            if priority == 0:
                priority = None
            else:
                priority = Priority(priority).name
            if status == 0:
                status = None
            else:
                status = Status(status)
            if parent_task_id == 0:
                parent_task_id = None

            print(priority)

            tasks = api.search_task(
                user=user,
                name=name,
                start_time=start_time,
                end_time=end_time,
                priority=priority,
                status=status,
                parent_task_id=parent_task_id,
                executor=executor
            )

            folders = []
            for task in tasks:
                users = [executors.user for executors in task.executors]
                if task.executors and user in users:
                    folders.append('executable')
                elif task.folder is not None:
                    folders.append(task.folder.id)
                else:
                    folders.append('without')
            print(folders)
            folder_task_list = list(zip(folders, tasks))

            return render(request, 'tracker/search_result.html',
                          {'folder_task_list': folder_task_list})
    else:
        form = SearchForm(user)

    return render(request, 'tracker/search.html', {'form': form})


@generate_task_plan
@show_notifications
def password_edit(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password has been changed successfully.')
            return redirect('home')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'tracker/home.html', {'form': form})
