from django.conf import settings
from tr_library.api.api import API


def get_api():
    database_settings = settings.DATABASES["default"]
    dialect = database_settings["DIALECT"]
    name = database_settings["NAME"]
    return API("{}:///{}".format(dialect, name))
