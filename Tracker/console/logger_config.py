import os
import logging

DEFAULT_LOGGER_CONFIG = {
    "enabled": True,
    "level": logging.DEBUG,
    "log_format": "%(asctime)s, %(name)s, [%(levelname)s]: %(message)s",
    "date_format": "%d.%m.%Y %H:%M:%S",
    "filename": "logger.log",
    "directory": "~/.tracker/"
}


def init_logger(logger, logger_config):
    enabled = DEFAULT_LOGGER_CONFIG["enabled"] or logger_config["enabled"]

    if not enabled:
        logger.disabled = True
    else:
        level = DEFAULT_LOGGER_CONFIG["level"] or logger_config["level"]
        log_format = (DEFAULT_LOGGER_CONFIG["log_format"] or
                      logger_config["log_format"])
        date_format = (DEFAULT_LOGGER_CONFIG["date_format"]
                       or logger_config["date_format"])
        filename = (DEFAULT_LOGGER_CONFIG["filename"]
                    or logger_config["filename"])
        directory = os.path.expanduser(DEFAULT_LOGGER_CONFIG["directory"] or
                                       logger_config["directory"])
        if not os.path.isdir(directory):
            os.mkdir(directory)

        path = os.path.join(directory, filename)
        file_handler = logging.FileHandler(path)
        formatter = logging.Formatter(log_format, date_format)
        file_handler.setFormatter(formatter)

        logger.setLevel(level)
        logger.addHandler(file_handler)
        logger.disabled = False
