import argparse
import sys
from datetime import datetime

import console.handlers as handlers
from tr_library.models.task import Status


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        print('error: %s\n' % message, file=sys.stderr)
        self.print_help()
        sys.exit(2)


def convert_str_to_datetime(text):
    try:
        return datetime.strptime(text, "%d.%m.%Y %H:%M")
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid date format")


def convert_str_to_priority(text):
    if text.lower() in ("low", "l", "1"):
        return "low"
    elif text.lower() in ("medium", "m", "2"):
        return "medium"
    elif text.lower() in ("high", "h", "3"):
        return "high"
    else:
        raise argparse.ArgumentTypeError("Invalid priority")


def convert_str_to_status(text):
    if text.lower() in ("todo", "1"):
        return Status.TODO
    elif text.lower() in ("in_progress", "in progress", "progress", "2"):
        return Status.IN_PROGRESS
    elif text.lower() in ("done", "ok", "3"):
        return Status.DONE
    elif text.lower() in ("failed", "fail", "4"):
        return Status.FAILED
    else:
        raise argparse.ArgumentTypeError("Invalid status")


def init_parser():
    parser = DefaultHelpParser(
        prog="tracker",
        description="Simple console tracker"
    )
    subparser = parser.add_subparsers(dest='object')
    subparser.required = True

    user_parser = subparser.add_parser(
        "user",
        help="Object for work with users"
    )
    create_user_parser(user_parser)

    task_parser = subparser.add_parser(
        "task",
        help="Object for work with tasks"
    )
    create_task_parser(task_parser)

    task_plan_parser = subparser.add_parser(
        "task_plan",
        help="Object for automatic creation of tasks"
    )
    create_task_plan_parser(task_plan_parser)

    folder_parser = subparser.add_parser(
        "folder",
        help="Object for storing tasks"
    )
    create_folder_parser(folder_parser)

    notification_parser = subparser.add_parser(
        "notification",
        help="Object reminding about task execution"
    )
    create_notification_parser(notification_parser)

    tag_parser = subparser.add_parser(
        "tag",
        help="Object for grouping tasks"
    )
    create_tag_parser(tag_parser)

    executor_parser = subparser.add_parser(
        "executor",
        help="Users who have been granted access to perform tasks of other users"
    )
    create_executor_parser(executor_parser)

    config_parser = subparser.add_parser(
        "config",
        help="Object for setting up the database and logger"
    )
    create_config_parser(config_parser)

    return parser.parse_args()


def create_user_parser(parser):
    user_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with user"
    )
    user_subparser.required = True

    user_change = user_subparser.add_parser(
        "change",
        help="Change user"
    )
    user_change.set_defaults(func=handlers.handler_user)
    user_change.add_argument(
        "username",
        help="Change user to username",
        type=str
    )

    user_show = user_subparser.add_parser(
        "show",
        help="Show username"
    )
    user_show.set_defaults(func=handlers.handler_user_show)


def create_task_parser(parser):
    task_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with task"
    )
    task_subparser.required = True

    task_add = task_subparser.add_parser(
        "add",
        help="Create task"
    )
    task_add.set_defaults(func=handlers.handler_task_add)
    task_add.add_argument(
        "name",
        help="Task name",
        type=str
    )
    task_add.add_argument(
        "-s", "--start_time",
        help="Task start time. %s" % datetime.now().strftime("%d.%m.%Y %H:%M"),
        type=convert_str_to_datetime
    )
    task_add.add_argument(
        "-e", "--end_time",
        help="Task end time. %s" % datetime.now().strftime("%d.%m.%Y %H:%M"),
        type=convert_str_to_datetime
    )
    task_add.add_argument(
        "-p", "--priority",
        help="Task priority",
        type=convert_str_to_priority
    )

    task_edit = task_subparser.add_parser(
        "edit",
        help="Edit task"
    )
    task_edit.set_defaults(func=handlers.handler_task_edit)
    task_edit.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )
    task_edit.add_argument(
        "-n", "--name",
        help="New task name",
        type=str
    )
    task_edit.add_argument(
        "-s", "--start_time",
        help="New task start time. %s" % datetime.now().strftime("%d.%m.%Y %H:%M"),
        type=convert_str_to_datetime
    )
    task_edit.add_argument(
        "-e", "--end_time",
        help="New task end time. %s" % datetime.now().strftime("%d.%m.%Y %H:%M"),
        type=convert_str_to_datetime
    )
    task_edit.add_argument(
        "-p", "--priority",
        help="New task priority",
        type=convert_str_to_priority
    )

    task_attach = task_subparser.add_parser(
        "attach",
        help="Attach task to another task"
    )
    task_attach.set_defaults(func=handlers.handler_task_attach)
    task_attach.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )
    task_attach.add_argument(
        "subtask_id",
        help="Subtask ID",
        type=int
    )

    task_detach = task_subparser.add_parser(
        "detach",
        help="Detach subtask from task"
    )
    task_detach.set_defaults(func=handlers.handler_task_detach)
    task_detach.add_argument(
        "subtask_id",
        help="Subtask ID",
        type=int
    )

    task_move = task_subparser.add_parser(
        "move",
        help="Move task to folder"
    )
    task_move.set_defaults(func=handlers.handler_task_move)
    task_move.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )
    task_move.add_argument(
        "folder_id",
        help="Folder ID",
        type=int
    )
    task_list = task_subparser.add_parser(
        "list",
        help="Show tasks with specified filter"
    )
    task_list.set_defaults(func=handlers.handler_task_list)
    task_list.add_argument(
        "-n", "--name",
        help="Tasks with a given name",
        type=str
    )
    task_list.add_argument(
        "-s", "--start_time",
        help=("Tasks with a given start time. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    task_list.add_argument(
        "-e", "--end_time",
        help=("Tasks with a given end time. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    task_list.add_argument(
        "-p", "--priority",
        help="Tasks with a given priority",
        type=convert_str_to_priority
    )
    task_list.add_argument(
        "-S", "--status",
        help="Tasks with a given status",
        type=convert_str_to_status
    )
    task_list.add_argument(
        "-f", "--folder_id",
        help="Tasks with a given folder ID",
        type=int
    )
    task_list.add_argument(
        "-P", "--parent_task_id",
        help="Tasks with a given parent task ID",
        type=int,
        dest="parent_task_id"
    )
    task_list.add_argument(
        "-t", "--task_id",
        help="Tasks with a given task plan ID",
        type=int,
        dest="task_plan_id"
    )
    task_list.add_argument(
        "-E", "--executor",
        help="Tasks where the user is the executor",
        action="store_true"
    )
    task_list.add_argument(
        "-T", "--tree",
        help="Task tree (ID)",
        type=int
    )

    task_show = task_subparser.add_parser(
        "show",
        help="Show task detail"
    )
    task_show.set_defaults(func=handlers.handler_task_show)
    task_show.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )

    task_complite = task_subparser.add_parser(
        "complite",
        help="Complete task and its subtasks"
    )
    task_complite.set_defaults(func=handlers.handler_task_complite)
    task_complite.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )

    task_delete = task_subparser.add_parser(
        "del",
        help="Delete task"
    )
    task_delete.set_defaults(func=handlers.handler_task_delete)
    task_delete.add_argument(
        "task_id",
        help="Task ID",
        type=int
    )
    task_delete.add_argument(
        "-t", "--tree",
        help="Delete a task with its subtasks",
        action="store_true"
    )


def create_task_plan_parser(parser):
    task_plan_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with task plan"
    )
    task_plan_subparser.required = True

    task_plan_add = task_plan_subparser.add_parser(
        "add",
        help="Create new task plan"
    )
    task_plan_add.set_defaults(func=handlers.handler_task_plan_add)
    task_plan_add.add_argument(
        "interval",
        help="Interval between the tasks being created. E.g. '2 day'",
        type=str
    )
    task_plan_add.add_argument(
        "task_id",
        help="Task id for creating a template",
        type=str
    )
    task_plan_add.add_argument(
        "-s", "--start_time",
        help=("The time after which the creation of tasks begins. %s"
              % datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    task_plan_add.add_argument(
        "-e", "--end_time",
        help=("The time after which to stop creating tasks. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    task_plan_add.add_argument(
        "-t", "--total_count",
        help="Count of tasks to create",
        type=int,
        dest="total_tasks_count"
    )

    task_plan_list = task_plan_subparser.add_parser(
        "list",
        help="Show task plan with specified filter"
    )
    task_plan_list.set_defaults(func=handlers.handler_task_plan_list)
    task_plan_list.add_argument(
        "-i", "--interval",
        help="Task plans with a given interval",
        type=str
    )
    task_plan_list.add_argument(
        "-t", "--total_tasks",
        help="Task plan with specific total tasks",
        type=int
    )
    task_plan_list.add_argument(
        "-c", "--created_tasks",
        help="Task plan with specific count of created tasks",
        type=int
    )
    task_plan_list.add_argument(
        "-l", "--last_created",
        help="Task plan with a specific time for creating the last task",
        type=convert_str_to_datetime
    )

    task_plan_show = task_plan_subparser.add_parser(
        "show",
        help="Show task plan detail"
    )
    task_plan_show.set_defaults(func=handlers.handler_task_plan_show)
    task_plan_show.add_argument(
        "task_plan_id",
        help="",
        type=int
    )

    task_plan_delete = task_plan_subparser.add_parser(
        "del",
        help="Delete task plan"
    )
    task_plan_delete.set_defaults(func=handlers.handler_task_plan_delete)
    task_plan_delete.add_argument(
        "task_plan_id",
        help="Task plan ID",
        type=int
    )


def create_folder_parser(parser):
    folder_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with folder"
    )
    folder_subparser.required = True

    folder_add = folder_subparser.add_parser(
        "add",
        help="Create new folder"
    )
    folder_add.set_defaults(func=handlers.handler_folder_add)
    folder_add.add_argument(
        "name",
        help="Folder name",
        type=str
    )
    folder_add.add_argument(
        "-p", "--parent_id",
        help="Parent folder ID",
        type=int,
        dest="parent_folder_id"
    )

    folder_edit = folder_subparser.add_parser(
        "edit",
        help="Edit folder"
    )
    folder_edit.set_defaults(func=handlers.handler_folder_edit)
    folder_edit.add_argument(
        "folder_id",
        help="Folder ID",
        type=int,
    )

    folder_edit.add_argument(
        "-n", "--name",
        help="New folder name",
        type=str
    )

    folder_list = folder_subparser.add_parser(
        "list",
        help="Show folders with specified filter"
    )
    folder_list.set_defaults(func=handlers.handler_folder_list)
    folder_list.add_argument(
        "-n", "--name",
        help="Folders with a given name",
        type=str
    )
    folder_list.add_argument(
        "-p", "--parent_id",
        help="Folders with a given parent folder ID",
        type=str,
        dest="parent_folder_id"
    )
    folder_list.add_argument(
        "-T", "--tree",
        help="Folder tree (ID)",
        type=int
    )

    folder_show = folder_subparser.add_parser(
        "show",
        help="Show folder detail"
    )
    folder_show.set_defaults(func=handlers.handler_folder_show)
    folder_show.add_argument(
        "folder_id",
        help="Folder ID",
        type=str
    )
    folder_show.add_argument(
        "-t", "--tasks",
        help="Show folder tasks",
        action="store_true"
    )

    folder_delete = folder_subparser.add_parser(
        "del",
        help="Delete folder"
    )
    folder_delete.set_defaults(func=handlers.handler_folder_delete)
    folder_delete.add_argument(
        "folder_id",
        help="Folder ID",
        type=int
    )
    folder_delete.add_argument(
        "-t", "--tree",
        help="Delete a folder with its subfolders",
        action="store_true"
    )


def create_notification_parser(parser):
    notification_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with notification"
    )
    notification_subparser.required = True

    notification_add = notification_subparser.add_parser(
        "add",
        help="Create new notification"
    )
    notification_add.set_defaults(func=handlers.handler_notification_add)
    notification_add.add_argument(
        "task_id",
        help="Task ID to which a notification should be added",
        type=int
    )
    notification_add.add_argument(
        "date",
        help=("Time of notification show. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    notification_add.add_argument(
        "-m", "--message",
        help="Notification message",
        type=str
    )

    notification_edit = notification_subparser.add_parser(
        "edit",
        help="Edit notification"
    )
    notification_edit.set_defaults(func=handlers.handler_notification_edit)
    notification_edit.add_argument(
        "notification_id",
        help="Notification ID",
        type=int
    )
    notification_edit.add_argument(
        "-d", "--date",
        help=("New notification date. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    notification_edit.add_argument(
        "-m", "--message",
        help="New message",
        type=str
    )

    notification_list = notification_subparser.add_parser(
        "list",
        help="Show notification with specified filter"
    )
    notification_list.set_defaults(func=handlers.handler_notification_list)
    notification_list.add_argument(
        "-d", "--date",
        help=("Notification with a given date. %s" %
              datetime.now().strftime("%d.%m.%Y %H:%M")),
        type=convert_str_to_datetime
    )
    notification_list.add_argument(
        "-s", "--shown",
        help="Notification with a given shown",
        action="store_true"
    )
    notification_list.add_argument(
        "-m", "--message",
        help="Notification with a given message",
        type=str
    )
    notification_list.add_argument(
        "-t", "--task_id",
        help="Notification with a given task ID",
        type=int
    )

    notification_show = notification_subparser.add_parser(
        "show",
        help="Show notification detail"
    )
    notification_show.set_defaults(func=handlers.handler_notification_show)
    notification_show.add_argument(
        "notification_id",
        help="Notification ID",
        type=int
    )

    notification_delete = notification_subparser.add_parser(
        "del",
        help="Delete notification"
    )
    notification_delete.set_defaults(func=handlers.handler_notification_delete)
    notification_delete.add_argument(
        "notification_id",
        help="Notification ID",
        type=int
    )


def create_tag_parser(parser):
    tag_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with tag"
    )
    tag_subparser.required = True

    tag_add = tag_subparser.add_parser(
        "add",
        help="Create new tag"
    )
    tag_add.set_defaults(func=handlers.handler_tag_add)
    tag_add.add_argument(
        "task_id",
        help="Task id to which to add a tag",
        type=int
    )
    tag_add.add_argument(
        "name",
        help="Tag name",
        type=str
    )
    tag_add.add_argument(
        "-t", "--tag_id",
        help="Tag id that will be added to the task",
        type=int
    )

    tag_edit = tag_subparser.add_parser(
        "edit",
        help="Edit tag"
    )
    tag_edit.set_defaults(func=handlers.handler_tag_edit)
    tag_edit.add_argument(
        "tag_id",
        help="Tag ID",
        type=int
    )
    tag_edit.add_argument(
        "name",
        help="New tag name",
        type=str
    )

    tag_list = tag_subparser.add_parser(
        "list",
        help="Show tag with specified filter"
    )
    tag_list.set_defaults(func=handlers.handler_tag_list)
    tag_list.add_argument(
        "-n", "--name",
        help="Tags with a given name",
        type=str
    )
    tag_list.add_argument(
        "-t", "--task_id",
        help="Tags with a given task ID",
        type=int
    )

    tag_show = tag_subparser.add_parser(
        "show",
        help="Show tag detail"
    )
    tag_show.set_defaults(func=handlers.handler_tag_show)
    tag_show.add_argument(
        "tag_id",
        help="Tag ID",
        type=int
    )

    tag_delete = tag_subparser.add_parser(
        "del",
        help="Delete tag"
    )
    tag_delete.set_defaults(func=handlers.handler_tag_delete)
    tag_delete.add_argument(
        "tag_id",
        help="Tag ID",
        type=int
    )
    tag_delete.add_argument(
        "-t", "--task_id",
        help="Task id where to delete the tag",
        type=int
    )


def create_executor_parser(parser):
    executor_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with executor"
    )
    executor_subparser.required = True

    executor_add = executor_subparser.add_parser(
        "add",
        help="Add executor to task"
    )
    executor_add.set_defaults(func=handlers.handler_executor_add)
    executor_add.add_argument(
        "task_id",
        help="Task ID to which the performer is add",
        type=str
    )
    executor_add.add_argument(
        "executor_user",
        help="Executor name",
        type=str
    )
    executor_add.add_argument(
        "-t", "--to_subtasks",
        help="Add executor to subtasks",
        action="store_true"
    )

    executor_list = executor_subparser.add_parser(
        "list",
        help="Show executor with specified filter"
    )
    executor_list.set_defaults(func=handlers.handler_executor_list)
    executor_list.add_argument(
        "-t", "--task_id",
        help="Executor for specific task",
        type=int
    )
    executor_list.add_argument(
        "-n", "--executor_user",
        help="Executor name",
        type=str
    )

    executor_show = executor_subparser.add_parser(
        "show",
        help="Show executor detail"
    )
    executor_show.set_defaults(func=handlers.handler_executor_show)
    executor_show.add_argument(
        "executor_id",
        help="Executor ID",
        type=int
    )

    executor_delete = executor_subparser.add_parser(
        "del",
        help="Delete executor from task"
    )
    executor_delete.set_defaults(func=handlers.handler_executor_delete)
    executor_delete.add_argument(
        "task_id",
        help="Task id that delete the executor",
        type=str
    )
    executor_delete.add_argument(
        "executor_user",
        help="Executor name",
        type=str
    )
    executor_delete.add_argument(
        "-t", "--to_subtasks",
        help="Delete executor from subtasks",
        action="store_true"
    )


def create_config_parser(parser):
    config_subparser = parser.add_subparsers(
        dest="action",
        help="Actions with config"
    )
    config_subparser.required = True

    config_edit = config_subparser.add_parser(
        "edit",
        help="Edit config"
    )

    config_edit.set_defaults(func=handlers.handler_config_edit)
    config_edit.add_argument(
        "section",
        help="Config section",
        type=str
    )
    config_edit.add_argument(
        "key",
        help="Custom field key",
        type=str
    )
    config_edit.add_argument(
        "value",
        help="Custom field value",
        type=str
    )

    config_change = config_subparser.add_parser(
        "change",
        help="Change config"
    )
    config_change.set_defaults(func=handlers.handler_config_change)
    config_change.add_argument(
        "new_path",
        help="New config path",
        type=str
    )

    config_show = config_subparser.add_parser(
        "show",
        help="Show config detail"
    )
    config_show.set_defaults(func=handlers.handler_config_show)
