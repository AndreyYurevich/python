import copy
import sys

from functools import wraps

from console.config import Config
from console.logger_config import init_logger
from console.handlers import handler_task_plan, handler_notifications
from console.arg_parser import init_parser
from tr_library.logger import get_logger
from tr_library.api.exceptions import LibraryError
from tr_library.api.api import API


def handle_command_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except LibraryError as e:
            print("\nError in library: %s\n" % e, file=sys.stderr)
            sys.exit(1)
        except KeyError as e:
            print("\nInvalid key: %s\n" % e, file=sys.stderr)
            sys.exit(1)
        except ValueError as e:
            print("\nInvalid value: %s\n" % e, file=sys.stderr)
            sys.exit(1)
        except Exception as e:
            print("\nInternal Error: %s\n" % e, file=sys.stderr)
            sys.exit(1)

    return wrapper


@handle_command_error
def main():
    config = Config()

    logger = config.get_config("logging")
    log = get_logger()
    init_logger(log, logger)

    database = config.get_config("database")
    library_api = API(database["database_url"])
    user_config = config.get_config("user")
    user = user_config["username"]

    handler_task_plan(user, library_api)
    handler_notifications(user, library_api)

    args = init_parser()
    args_dict = vars(copy.copy(args))
    args_dict.pop("func")
    args_dict.pop("action")
    args_dict.pop("object")

    if getattr(args, "object") in ["user", "config"]:
        args.func(config, args_dict)
    else:
        args.func(user, library_api, args_dict)


if __name__ == "__main__":
    main()
