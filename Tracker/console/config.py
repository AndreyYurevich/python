import os
import configparser
import getpass

from console.logger_config import DEFAULT_LOGGER_CONFIG

DEFAULT_CONFIG_DIRECTORY = os.path.expanduser("~/.tracker/")
DEFAULT_DATABASE_URL = ''.join(["sqlite:///",
                                DEFAULT_CONFIG_DIRECTORY,
                                "tracker.db"])


class Config:
    def __init__(self):
        self._config = configparser.ConfigParser(interpolation=None)

        self.path = os.path.join(
            DEFAULT_CONFIG_DIRECTORY,
            "config.ini"
        )
        if os.path.exists(self.path):
            self._config.read(self.path)
        else:
            self._create_default_config()

    def _write_config(self):
        directory = os.path.dirname(self.path)
        if not os.path.isdir(directory):
            os.mkdir(directory)

        with open(self.path, "w") as config_file:
            self._config.write(config_file)

    def _create_default_config(self):
        self._config["logging"] = DEFAULT_LOGGER_CONFIG
        self._config["user"] = {"username": getpass.getuser()}
        self._config["database"] = {"database_url": DEFAULT_DATABASE_URL}

        self._write_config()

    def change_config(self, new_path):
        if os.path.exists(new_path):
            self._config.read(new_path)
            self._write_config()
        else:
            raise ValueError("New config not found")

    def set_config(self, section, key, value):
        self._config.set(section, key, value)

        self._write_config()

    def get_config(self, key):
        try:
            return dict(self._config.items(key))
        except configparser.Error:
            raise KeyError("Config key not found")
