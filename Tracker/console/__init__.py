"""
    This package contains all functionality to interact
    with library the command line.

    Modules:
        arg_parser.py - describes initialization of command-line parsers,
                        string conversion functions.
        config.py - describes class used to create,
                    replace and interact with the config.
        handlers.py - describes logic for handling actions on command line.
        logger_config.py - describes initialization of logger.
        main.py - is entry point when you run commands, describes
                  the steps to call the program from the command line.
"""