import textwrap


def handler_task_plan(user, library_api):
    library_api.generate_a_task_plan(user)


def handler_notifications(user, library_api):
    notifications = library_api.show_notifications(user)

    if notifications:
        print("\nMissed notifications:")

        for notification in notifications:
            print(str(notification))


def handler_user(config, args):
    config.set_config("user", "username", args.get("username"))
    print("Name successfully changed")


def handler_user_show(config, args):
    name = config.get_config("user")
    print("Name: %s" % name.get("username"))


def handler_task_add(user, library_api, args):
    id = library_api.create_task(user, **args)
    print("The task was successfully added. ID = %s" % id)


def handler_task_edit(user, library_api, args):
    count = sum(val is not None for val in args.values())
    if count > 1:
        library_api.edit_task(user, **args)
        print("Task %s edited" % args.get("task_id"))
    else:
        print("Specify the fields for editing")


def handler_task_attach(user, library_api, args):
    library_api.attach_subtask(
        user=user, task_id=args.get("task_id"),
        subtask_id=args.get("subtask_id")
    )
    print(("Task (ID: %s) is attached to task (ID: %s)"
           % (args.get("subtask_id"), args.get("task_id"))))


def handler_task_detach(user, library_api, args):
    library_api.detach_subtask(user=user, subtask_id=args.get("subtask_id"))
    print("Subtask (ID: %s) is attached" % args.get("subtask_id"))


def handler_task_move(user, library_api, args):
    if args.get("folder_id") is None:
        library_api.delete_task_from_folder(user, **args)
        print("Tasks successfully delete from folder")
    else:
        library_api.add_task_to_folder(user, **args)
        print("The task and its tree were successfully moved to folder")


def handler_task_list(user, library_api, args):
    if args.get("tree"):
        tree = library_api.get_task_tree(user, args.get("tree"))
        _print_task_tree(tree)
    else:
        args.pop("tree")
        tasks = library_api.get_tasks(user, **args)

        for task in tasks:
            print("<Task ID: %d, name: %s>" % (task.id, task.name))


def _print_task_tree(tree, depth=0):
    for task in tree:
        if isinstance(task, list):
            _print_task_tree(task, depth + 1)
        else:
            print(textwrap.indent(("<Task ID: %d, name: %s>" %
                                   (task.id, task.name)), " " * 4 * depth))


def handler_task_show(user, library_api, args):
    task = library_api.get_task(user, args.get("task_id"))
    print(task)


def handler_task_complite(user, library_api, args):
    library_api.complete_task(user, args.get("task_id"))
    print("Task (ID: %s) is completed" % args.get("task_id"))


def handler_task_delete(user, library_api, args):
    if args.get("tree"):
        library_api.delete_task(user=user, task_id=args.get("task_id"))
        print("Tasks successfully deleted")
    else:
        library_api.delete_task(
            user=user,
            task_id=args.get("task_id"),
            with_subtasks=False
        )
        print("Task successfully deleted")


def handler_task_plan_add(user, library_api, args):
    count = sum(1 for s in args.get("interval").split(" "))
    if count != 2:
        print("Invalid interval")
    else:
        id = library_api.create_task_plan(user, **args)
        print("The task plan was successfully added. ID = %s" % id)


def handler_task_plan_list(user, library_api, args):
    task_plans = library_api.get_task_plans(user, **args)

    for task_plan in task_plans:
        print("<Task plan ID: %d>" % task_plan.id)


def handler_task_plan_show(user, library_api, args):
    task_plan = library_api.get_task_plan(user, **args)
    print(task_plan)


def handler_task_plan_delete(user, library_api, args):
    library_api.delete_task_plan(user, **args)
    print("Task plan successfully deleted")


def handler_folder_add(user, library_api, args):
    id = library_api.create_folder(user, **args)
    print("The folder was successfully added. ID = %s" % id)


def handler_folder_edit(user, library_api, args):
    count = sum(val is not None for val in args.values())
    if count > 1:
        library_api.edit_folder(user, **args)
        print("Folder %s edited" % args.get("folder_id"))
    else:
        print("Specify the fields for editing")


def handler_folder_list(user, library_api, args):
    if args.get("tree"):
        tree = library_api.get_folder_tree(user, args.get("tree"))
        _print_folder_tree(tree)
    else:
        args.pop("tree")
        folders = library_api.get_folders(user, **args)

        for folder in folders:
            print("<Folder ID: %d, name: %s>" % (folder.id, folder.name))


def _print_folder_tree(tree, depth=0):
    for folder in tree:
        if isinstance(folder, list):
            _print_folder_tree(folder, depth + 1)
        else:
            print(textwrap.indent(("<Folder ID: %d, name: %s>"
                                   % (folder.id, folder.name), " "
                                   * 4 * depth)))


def handler_folder_show(user, library_api, args):
    folder = library_api.get_folder(user, args.get("folder_id"))
    print(folder)

    if args.get("tasks"):
        tasks = folder.tasks
        group = []
        for task in tasks:
            if task.group == 0:
                id = task.id
            else:
                id = task.group
            if id not in group:
                group.append(id)

        for id in group:
            tree = library_api.get_task_tree(user, id)
            _print_task_tree(tree)


def handler_folder_delete(user, library_api, args):
    if args.get("tree"):
        library_api.delete_folder(user=user, folder_id=args.get("folder_id"))
        print("Folders successfully deleted")
    else:
        library_api.delete_folder(
            user=user,
            folder_id=args.get("folder_id"),
            with_subfolders=False
        )
        print("Folder successfully deleted")


def handler_notification_add(user, library_api, args):
    id = library_api.add_notification(user, **args)
    print("The notification was successfully added. ID = %s" % id)


def handler_notification_edit(user, library_api, args):
    count = sum(val is not None for val in args.values())
    if count > 1:
        notification = library_api.get_notification(
            user, args.get("notification_id")
        )
        if notification.shown:
            print("Notification already shown")
        else:
            library_api.edit_notification(user, **args)
            print("Notification %s edited" % args.get("notification_id"))
    else:
        print("Specify the notification for editing")


def handler_notification_list(user, library_api, args):
    notifications = library_api.get_notifications(user, **args)

    for notification in notifications:
        print(("<Notification ID: %d, task ID: %s>"
               % (notification.id, notification.task_id)))


def handler_notification_show(user, library_api, args):
    notification = library_api.get_notification(
        user, args.get("notification_id")
    )
    print(notification)


def handler_notification_delete(user, library_api, args):
    library_api.delete_notification(user, **args)
    print("Notification successfully deleted")


def handler_tag_add(user, library_api, args):
    id = library_api.add_tag(user, **args)
    print("The tag was successfully added. ID = %s" % id)


def handler_tag_edit(user, library_api, args):
    library_api.edit_tag(user, **args)
    print("Tag %s edited" % args.get("tag_id"))


def handler_tag_list(user, library_api, args):
    tags = library_api.get_tags(user, **args)

    for tag in tags:
        print("<Tag ID: %d, name: %s>" % (tag.id, tag.name))


def handler_tag_show(user, library_api, args):
    tag = library_api.get_tag(user, **args)
    print(tag)


def handler_tag_delete(user, library_api, args):
    library_api.delete_tag(user, **args)

    if args.get("task_id"):
        print("Tag successfully deleted from task")
    else:
        print("Tag successfully deleted from tasks")


def handler_executor_add(user, library_api, args):
    id = library_api.add_executor_to_task(user, **args)
    print("The executor was successfully added. ID = %s" % id)


def handler_executor_list(user, library_api, args):
    executors = library_api.get_executors(user, **args)

    for executor in executors:
        print("<Executor ID: %d, name: %s>" % (executor.id, executor.user))


def handler_executor_show(user, library_api, args):
    executor = library_api.get_executor(**args)
    print(executor)


def handler_executor_delete(user, library_api, args):
    library_api.delete_executor_from_task(user, **args)
    if args.get("to_subtasts"):
        print("Executor successfully deleted from tasks")
    else:
        print("Executor successfully deleted from task")


def handler_config_edit(config, args):
    config.set_config(**args)
    print("Config edited. New value: %s.%s = %s" % (args.get("section"),
                                                    args.get("key"),
                                                    args.get("value")))


def handler_config_change(config, args):
    config.change_config(**args)
    print("Config successfully changed")


def handler_config_show(config, args):
    print()
    with open(config.path, 'r') as config_file:
        print(config_file.read())
