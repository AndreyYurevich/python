from setuptools import setup, find_packages


setup(
    name="Tracker",
    version='0.1',
    author="Andrey_Yurevich",
    author_email="and9019468@yandex.ru",
    install_requires=["SQLAlchemy", "python-dateutil"],
    packages=find_packages(),
    entry_points={"console_scripts": ["tracker = console.main:main"]},
    test_suite='tr_library.tests'
)
