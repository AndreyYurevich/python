"""
    A simple library for working with a tracker library.

    Packages:
        api - contains api for working with database models.
        models - contains models that are used in the library.
        tests - contains tests that verify the library's operability.

    Modules:
        database.py - contains function for connecting to database.


    Usage:
        1) Import library.
        2) Create object of library api using connection_string.
        3) Use api methods for their own needs.

    Example:
        >>> from library.api.api import API
        >>> connection_string = "sqlite:///db"
        >>> api = API(connection_string)
        >>> user = "user"
        
        >>> task_id = api.create_task(user=user, name=name)
        >>> subtask_id = api.create_task(user=user, name="subtask")
        >>> tag_id = api.add_tag(user=user, task_id=task_id, name="tag")
        
        >>> api.attach_subtask(user, task_id, subtask_id)
        >>> task = api.get_task(user, task_id)
        >>> subtask = api.get_task(user, subtask_id)
        >>> print(task)

        ID: 1, name: task
        Start time: 12.09.2018 00:02
        Priority: LOW
        Status: IN_PROGRESS
        Tags: [tag]
        Subtasks ID: [2]
        
        >>> print(subtask_id)
        2
        
        >>> print(tags)
        [<library.models.tag.Tag object at 0x10bb16630>]
        
        >>> api.complete_task(user, task_id)
        >>> print(task.status)
        Status.DONE
        >>> print(subtask.status)
        Status.DONE

        
"""