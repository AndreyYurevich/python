import os
import unittest
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

from tr_library.api.api import API
from tr_library.models.task import Task, Status, Priority
from tr_library.models.folder import Folder

TEST_USER_NAME = "test_user"
TEST_INVALID_USER_NAME = "invalid_user"
TEST_EXECUTOR_NAME = "test_executor"
TEST_INVALID_EXECUTOR_NAME = "invalid_executor"
TEST_TASK_NAME = "test_task"
TEST_INVALID_TASK_NAME = "invalid_task"
TEST_EDITED_TASK = "edited_task"
TEST_INVALID_EDITED_TASK = "invalid_edited_task"
TEST_TAG_NAME = "test_tag"
TEST_INVALID_TAG_NAME = "invalid_tag"
TEST_FOLDER_NAME = "test_folder"
TEST_INVALID_FOLDER_NAME = "invalid_folder"
TEST_EDITED_FOLDER = "edited_folder"
TEST_INVALID_EDITED_FOLDER = "invalid_edited_folder"
TEST_INVALID_ID = "0"
DATABASE_NAME = "database_test"
CONNECTION_STRING = "sqlite:///" + DATABASE_NAME


class TestApi(unittest.TestCase):
    def setUp(self):
        self.api = API(CONNECTION_STRING)

    def tearDown(self):
        os.remove(DATABASE_NAME)

    def test_create_task(self):
        task_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        tasks_len = len(self.api.get_tasks(TEST_USER_NAME))

        self.assertEqual(tasks_len, 1)

        task = self.api.get_task(TEST_USER_NAME, task_id)

        self.assertIsInstance(task, Task)

        self.assertEqual(task.group, 0)
        self.assertEqual(task.user, TEST_USER_NAME)
        self.assertEqual(task.name, TEST_TASK_NAME)
        self.assertEqual(task.end_time, None)
        self.assertEqual(task.priority, Priority.LOW)
        self.assertEqual(task.status, Status.IN_PROGRESS)
        self.assertEqual(len(task.subtasks), 0)
        self.assertEqual(len(task.tags), 0)
        self.assertEqual(task.task_plan, None)
        self.assertEqual(len(task.notifications), 0)
        self.assertEqual(len(task.executors), 0)
        self.assertEqual(task.folder, None)

    def test_edit_task(self):
        task_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.edit_task(
            user=TEST_USER_NAME,
            task_id=task_id,
            name=TEST_EDITED_TASK,
            start_time=datetime(2000, 1, 1),
            end_time=datetime(2020, 1, 1),
            priority="High"
        )

        task = self.api.get_task(TEST_USER_NAME, task_id)

        self.assertEqual(task.name, TEST_EDITED_TASK)
        self.assertEqual(task.start_time, datetime(2000, 1, 1))
        self.assertEqual(task.end_time, datetime(2020, 1, 1))
        self.assertEqual(task.priority, Priority.HIGH)

    def test_attach_subtask(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task4_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task3_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task2_id,
            subtask_id=task4_id
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)
        task4 = self.api.get_task(TEST_USER_NAME, task4_id)

        self.assertEqual(task1.group, 0)
        self.assertEqual(task2.group, task1_id)
        self.assertEqual(task3.group, task1_id)
        self.assertEqual(task4.group, task1_id)
        self.assertEqual(len(task1.subtasks), 2)
        self.assertEqual(len(task2.subtasks), 1)
        self.assertEqual(len(task3.subtasks), 0)
        self.assertEqual(task2.parent_task_id, task1_id)
        self.assertEqual(task3.parent_task_id, task1_id)
        self.assertEqual(task4.parent_task_id, task2_id)

    def test_detach_subtask(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task2_id,
            subtask_id=task3_id
        )

        self.api.detach_subtask(
            user=TEST_USER_NAME,
            subtask_id=task2_id
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)

        self.assertEqual(len(task1.subtasks), 0)
        self.assertEqual(len(task2.subtasks), 1)
        self.assertEqual(task2.group, 0)
        self.assertEqual(task3.group, task2_id)
        self.assertEqual(task2.parent_task_id, None)
        self.assertEqual(task3.parent_task_id, task2_id)

    def test_complite_task(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        tasks = self.api.get_tasks(TEST_USER_NAME, status=Status.IN_PROGRESS)

        self.assertEqual(task1.status, Status.IN_PROGRESS)
        self.assertEqual(task2.status, Status.IN_PROGRESS)
        self.assertEqual(len(tasks), 2)

        self.api.complete_task(TEST_USER_NAME, task1_id)

        tasks = self.api.get_tasks(TEST_USER_NAME, status=Status.DONE)

        self.assertEqual(task1.status, Status.DONE)
        self.assertEqual(task2.status, Status.DONE)
        self.assertEqual(len(tasks), 2)

    def test_complite_task_as_executor(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task4_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task3_id,
            subtask_id=task4_id
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)
        task4 = self.api.get_task(TEST_USER_NAME, task4_id)

        self.api.add_executor_to_task(
            user=TEST_USER_NAME,
            task_id=task1_id,
            executor_user=TEST_EXECUTOR_NAME,
            to_subtasks=False
        )
        self.api.add_executor_to_task(
            user=TEST_USER_NAME,
            task_id=task3_id,
            executor_user=TEST_EXECUTOR_NAME,
            to_subtasks=True
        )

        self.api.complete_task(TEST_EXECUTOR_NAME, task1_id)
        self.api.complete_task(TEST_EXECUTOR_NAME, task3_id)

        self.assertEqual(task1.status, Status.DONE)
        self.assertEqual(task2.status, Status.IN_PROGRESS)
        self.assertEqual(task3.status, Status.DONE)
        self.assertEqual(task4.status, Status.DONE)

    def test_get_task_tree(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task4_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task5_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task3_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task2_id,
            subtask_id=task4_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task2_id,
            subtask_id=task5_id
        )

        task_tree = self.api.get_task_tree(TEST_USER_NAME, task1_id)

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)
        task4 = self.api.get_task(TEST_USER_NAME, task4_id)
        task5 = self.api.get_task(TEST_USER_NAME, task5_id)

        self.assertEqual(task_tree, [task1, [task2, [task4], [task5]], [task3]])

    def test_delete_task(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )
        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task2_id,
            subtask_id=task3_id
        )

        self.api.add_tag(user=TEST_USER_NAME, task_id=task2_id, name=TEST_TAG_NAME)
        self.api.add_notification(
            user=TEST_USER_NAME,
            date=datetime.now() + relativedelta(hours=1),
            task_id=task2_id
        )
        self.api.add_executor_to_task(TEST_USER_NAME, task2_id, TEST_EXECUTOR_NAME)

        self.api.delete_task(TEST_USER_NAME, task2_id, with_subtasks=False)

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)

        self.assertEqual(len(task1.subtasks), 0)
        self.assertEqual(task3.parent_task_id, None)
        self.assertEqual(task3.group, 0)

        tasks = self.api.get_tasks(TEST_USER_NAME)
        tags = self.api.get_tags(TEST_USER_NAME)
        notifications = self.api.get_notifications(TEST_USER_NAME)

        self.assertEqual(len(tasks), 2)
        self.assertEqual(len(tags), 0)
        self.assertEqual(len(notifications), 0)

    def test_create_folder(self):
        folder1_id = self.api.create_folder(TEST_USER_NAME, TEST_FOLDER_NAME)
        folder2_id = self.api.create_folder(TEST_USER_NAME,
                                            TEST_FOLDER_NAME, folder1_id)

        folder1 = self.api.get_folder(TEST_USER_NAME, folder1_id)
        folder2 = self.api.get_folder(TEST_USER_NAME, folder2_id)

        self.assertIsInstance(folder1, Folder)
        self.assertIsInstance(folder2, Folder)
        self.assertEqual(folder1.subfolders[0].id, folder2_id)
        self.assertEqual(folder2.parent_folder_id, folder1_id)

    def test_add_task_to_folder(self):
        folder_id = self.api.create_folder(TEST_USER_NAME, TEST_FOLDER_NAME)

        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )

        self.api.add_task_to_folder(TEST_USER_NAME, task2_id, folder_id)

        folder = self.api.get_folder(TEST_USER_NAME, folder_id)
        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        tasks = self.api.get_tasks(TEST_USER_NAME, folder_id=folder_id)

        self.assertEqual(len(tasks), 2)
        self.assertEqual(len(folder.tasks), 2)
        self.assertEqual(task1.folder, folder)
        self.assertEqual(task2.folder, folder)

    def test_edit_folder(self):
        folder_id = self.api.create_folder(TEST_USER_NAME, TEST_FOLDER_NAME)

        self.api.edit_folder(
            user=TEST_USER_NAME,
            folder_id=folder_id,
            name=TEST_EDITED_FOLDER
        )

        folder = self.api.get_folder(TEST_USER_NAME, folder_id)

        self.assertEqual(folder.name, TEST_EDITED_FOLDER)

    def test_delete_task_from_folder(self):
        folder_id = self.api.create_folder(TEST_USER_NAME, TEST_FOLDER_NAME)

        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )

        self.api.add_task_to_folder(TEST_USER_NAME, task2_id, folder_id)

        self.api.delete_task_from_folder(TEST_USER_NAME, task2_id)

        folder = self.api.get_folder(TEST_USER_NAME, folder_id)
        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)

        self.assertEqual(len(folder.tasks), 0)
        self.assertIsNone(task1.folder)
        self.assertIsNone(task2.folder)

    def test_delete_folder(self):
        folder_id = self.api.create_folder(TEST_USER_NAME, TEST_FOLDER_NAME)

        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        self.api.attach_subtask(
            user=TEST_USER_NAME,
            task_id=task1_id,
            subtask_id=task2_id
        )

        self.api.add_task_to_folder(TEST_USER_NAME, task2_id, folder_id)

        self.api.delete_folder(TEST_USER_NAME, folder_id)

        folders = self.api.get_folders(TEST_USER_NAME)
        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)

        self.assertEqual(len(folders), 0)
        self.assertIsNone(task1.folder)
        self.assertIsNone(task2.folder)

    def test_add_tag(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        tag1_id = self.api.add_tag(
            user=TEST_USER_NAME,
            task_id=task1_id,
            name=TEST_TAG_NAME
        )
        tag2_id = self.api.add_tag(
            user=TEST_USER_NAME,
            task_id=task2_id,
            tag_id=tag1_id
        )

        self.assertEqual(tag1_id, tag2_id)

        tags1 = self.api.get_tags(user=TEST_USER_NAME)
        tags2 = self.api.get_tags(user=TEST_USER_NAME, name=TEST_TAG_NAME)
        tags3 = self.api.get_tags(user=TEST_USER_NAME, task_id=task1_id)
        tag = self.api.get_tag(TEST_USER_NAME, tag1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)

        self.assertEqual(len(tags1), 1)
        self.assertEqual(len(tags2), 1)
        self.assertEqual(len(tags3), 1)
        self.assertEqual(tag.tasks[0].id, tag1_id)
        self.assertEqual(task2.tags[0], tag)

    def test_delete_tag(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task3_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)

        tag1_id = self.api.add_tag(
            user=TEST_USER_NAME,
            task_id=task1_id,
            name=TEST_TAG_NAME
        )
        self.api.add_tag(
            user=TEST_USER_NAME,
            task_id=task2_id,
            tag_id=tag1_id
        )
        self.api.add_tag(
            user=TEST_USER_NAME,
            task_id=task3_id,
            tag_id=tag1_id
        )

        self.api.delete_tag(TEST_USER_NAME, tag1_id, task1_id)

        tags = self.api.get_tags(user=TEST_USER_NAME)
        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        task3 = self.api.get_task(TEST_USER_NAME, task3_id)

        self.assertEqual(len(task1.tags), 0)
        self.assertEqual(task2.tags[0].id, tag1_id)
        self.assertEqual(task3.tags[0].id, tag1_id)

        self.api.delete_tag(TEST_USER_NAME, tag1_id)

        self.assertEqual(len(task2.tags), 0)
        self.assertEqual(len(task3.tags), 0)

    def test_add_notification(self):
        task_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        notification_id = self.api.add_notification(
            user=TEST_USER_NAME,
            date=datetime.now() + relativedelta(seconds=0.1),
            task_id=task_id
        )

        notifications = self.api.get_notifications(TEST_USER_NAME)
        task = self.api.get_task(TEST_USER_NAME, task_id)

        self.assertEqual(len(notifications), 1)
        self.assertEqual(task.notifications[0].id, notification_id)
        self.assertEqual(notifications[0].id, task_id)
        self.assertIsNotNone(notifications[0].date)

    def test_show_notification(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        notification1_id = self.api.add_notification(
            user=TEST_USER_NAME,
            date=datetime.now() + relativedelta(seconds=0.1),
            task_id=task1_id
        )
        notification2_id = self.api.add_notification(
            user=TEST_USER_NAME,
            date=datetime.now() + relativedelta(seconds=0.1),
            task_id=task2_id
        )

        time.sleep(0.1)

        self.api.show_notifications(TEST_USER_NAME)

        notification1 = self.api.get_notification(TEST_USER_NAME, notification1_id)
        notification2 = self.api.get_notification(TEST_USER_NAME, notification2_id)

        self.assertEqual(notification1.shown, True)
        self.assertEqual(notification2.shown, True)

    def test_delete_notification(self):
        task_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        notification_id = self.api.add_notification(
            user=TEST_USER_NAME,
            date=datetime.now() + relativedelta(seconds=0.1),
            task_id=task_id
        )

        self.api.delete_notification(TEST_USER_NAME, notification_id)

        task = self.api.get_task(TEST_USER_NAME, task_id)
        notifications = self.api.get_notifications(TEST_USER_NAME)

        self.assertEqual(len(notifications), 0)
        self.assertEqual(len(task.notifications), 0)

    def test_create_task_template(self):
        task_id = self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME,
            start_time=datetime.now(),
            end_time=datetime.now() + relativedelta(hours=1),
            priority="Low"
        )
        task_template_id = self.api.create_task_template(TEST_USER_NAME, task_id)

        task = self.api.get_task(TEST_USER_NAME, task_id)
        task_template = self.api.get_task_template(TEST_USER_NAME, task_template_id)

        self.assertEqual(task.user, task_template.user)
        self.assertEqual(task.name, task_template.name)
        self.assertEqual(task.start_time, task_template.start_time)
        self.assertEqual(task.end_time, task_template.end_time)
        self.assertEqual(task.priority, task_template.priority)
        self.assertIsNone(task_template.task_plan)

    def test_delete_task_template(self):
        task_id = self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME,
            start_time=datetime.now(),
            end_time=datetime.now() + relativedelta(hours=1),
            priority="Low"
        )
        task_template_id = self.api.create_task_template(TEST_USER_NAME, task_id)

        self.api.delete_task_template(TEST_USER_NAME, task_template_id)

        tasks = self.api.get_tasks(TEST_USER_NAME)
        task_templates = self.api.get_task_templates(TEST_USER_NAME)

        self.assertEqual(len(task_templates), 0)
        self.assertEqual(len(tasks), 1)

    def test_add_executor_to_task(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        self.api.attach_subtask(TEST_USER_NAME, task1_id, task2_id)
        executor_id = self.api.add_executor_to_task(
            user=TEST_USER_NAME,
            task_id=task1_id,
            executor_user=TEST_EXECUTOR_NAME,
            to_subtasks=True
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        executor = self.api.get_executor(executor_id)

        self.assertEqual(len(task1.executors), 1)
        self.assertEqual(task1.executors[0].id, executor_id)
        self.assertEqual(len(task2.executors), 1)
        self.assertEqual(task2.executors[0].id, executor_id)
        self.assertEqual(len(executor.tasks), 2)
        self.assertEqual(executor.tasks[0].id, task1_id)
        self.assertEqual(executor.tasks[1].id, task2_id)

    def test_delete_executor_from_task(self):
        task1_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task2_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        self.api.attach_subtask(TEST_USER_NAME, task1_id, task2_id)
        executor_id = self.api.add_executor_to_task(
            user=TEST_USER_NAME,
            task_id=task1_id,
            executor_user=TEST_EXECUTOR_NAME,
            to_subtasks=True
        )

        self.api.delete_executor_from_task(
            user=TEST_USER_NAME,
            task_id=task1_id,
            executor_user=TEST_EXECUTOR_NAME,
            to_subtasks=False
        )

        task1 = self.api.get_task(TEST_USER_NAME, task1_id)
        task2 = self.api.get_task(TEST_USER_NAME, task2_id)
        executor = self.api.get_executor(executor_id)

        self.assertEqual(len(task1.executors), 0)
        self.assertEqual(len(task2.executors), 1)
        self.assertEqual(len(executor.tasks), 1)
        self.assertEqual(executor.tasks[0].id, task2_id)

    def test_create_task_plan(self):
        task_id = self.api.create_task(TEST_USER_NAME, TEST_TASK_NAME)
        task_plan_id = self.api.create_task_plan(
            user=TEST_USER_NAME,
            interval="1 h",
            task_id=task_id,
            start_time=datetime.now() + relativedelta(seconds=1),
            end_time=datetime.now() + relativedelta(seconds=1),
            total_tasks_count=3
        )

        task_plan = self.api.get_task_plan(TEST_USER_NAME, task_plan_id)
        task_plans = self.api.get_task_templates(TEST_USER_NAME)
        task_templates = self.api.get_task_templates(TEST_USER_NAME)

        self.assertEqual(len(task_plans), 1)
        self.assertEqual(len(task_templates), 1)
        self.assertEqual(task_plan.task_template, task_templates[0])
        self.assertEqual(task_plan.user, TEST_USER_NAME)
        self.assertEqual(task_plan.interval, relativedelta(hours=1))
        self.assertIsNotNone(task_plan.start_time)
        self.assertIsNotNone(task_plan.end_time)
        self.assertEqual(task_plan.total_tasks, 3)
        self.assertEqual(len(task_plan.tasks), 0)
        self.assertEqual(task_plan.created_tasks, 0)
        self.assertIsNotNone(task_plan.last_created)

    def test_generate_a_task_plan(self):
        start_time = datetime.now()
        task_id = self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME,
            start_time=start_time,
            end_time=start_time + relativedelta(hours=2)
        )
        task_plan1_id = self.api.create_task_plan(
            user=TEST_USER_NAME,
            interval="1 sec",
            task_id=task_id,
            start_time=start_time + relativedelta(seconds=1),
            end_time=start_time + relativedelta(days=2),
            total_tasks_count=3
        )
        task_plan2_id = self.api.create_task_plan(
            user=TEST_USER_NAME,
            interval="3 sec",
            task_id=task_id,
            start_time=start_time + relativedelta(seconds=1),
            end_time=start_time + relativedelta(days=2),
            total_tasks_count=4
        )

        time.sleep(1)

        task_plan1 = self.api.get_task_plan(TEST_USER_NAME, task_plan1_id)
        task_plan2 = self.api.get_task_plan(TEST_USER_NAME, task_plan2_id)

        self.api.generate_a_task_plan(TEST_USER_NAME)

        tasks = self.api.get_tasks(TEST_USER_NAME)

        self.assertEqual(len(task_plan1.tasks), 1)
        self.assertEqual(len(task_plan2.tasks), 1)
        self.assertEqual(
            relativedelta(task_plan1.tasks[0].end_time,
                          task_plan1.tasks[0].start_time),
            relativedelta(hours=2),
        )
        self.assertEqual(
            relativedelta(task_plan2.tasks[0].end_time,
                          task_plan2.tasks[0].start_time),
            relativedelta(hours=2),
        )
        self.assertEqual(len(tasks), 3)

    def test_delete_task_plan(self):
        start_time = datetime.now()
        task_id = self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME,
            start_time=start_time,
            end_time=start_time + relativedelta(hours=2)
        )
        task_plan1_id = self.api.create_task_plan(
            user=TEST_USER_NAME,
            interval="1 min",
            task_id=task_id,
            start_time=start_time + relativedelta(seconds=1),
            end_time=start_time + relativedelta(days=2),
            total_tasks_count=3
        )

        task_plan1 = self.api.get_task_plan(TEST_USER_NAME, task_plan1_id)
        task_templete_id = task_plan1.task_template_id

        self.assertIsNotNone(task_templete_id)

        self.api.delete_task_plan(TEST_USER_NAME, task_plan1_id)

        task_templetes = self.api.get_task_templates(TEST_USER_NAME)
        task_plans = self.api.get_task_plans(TEST_USER_NAME)

        self.assertEqual(len(task_templetes), 0)
        self.assertEqual(len(task_plans), 0)

    def test_search_task(self):
        start_time = datetime.now()
        task_id = self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME,
            start_time=start_time,
            end_time=start_time + relativedelta(days=2),
            priority="High"
        )

        tasks1 = self.api.search_task(
            user=TEST_USER_NAME,
            start_time=start_time
        )
        tasks2 = self.api.search_task(
            user=TEST_USER_NAME,
            end_time=start_time + relativedelta(days=2)
        )
        tasks3 = self.api.search_task(
            user=TEST_USER_NAME,
            priority="High"
        )
        tasks4 = self.api.search_task(
            user=TEST_USER_NAME,
            name="task"
        )
        tasks5 = self.api.search_task(
            user=TEST_USER_NAME,
            name="test"
        )
        tasks6 = self.api.search_task(
            user=TEST_USER_NAME,
            priority="low"
        )

        self.assertEqual(len(tasks1), 1)
        self.assertEqual(len(tasks2), 1)
        self.assertEqual(len(tasks3), 1)
        self.assertEqual(len(tasks4), 1)
        self.assertEqual(len(tasks5), 1)
        self.assertEqual(len(tasks6), 0)


if __name__ == "__main__":
    unittest.main()
