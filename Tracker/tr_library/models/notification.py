"""
    This module contains a notification model
    that is used to remind of task execution.
"""

from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    DateTime,
    String,
    ForeignKey
)

from tr_library.database import Base


class Notification(Base):
    """
        Notification model:

        Attributes:
            id - notification identifier.
            user - the creator of the notification.
            date - time of notification show.
            shown - flag indicating whether the notification was displayed.
            message - notification message.
            task_id - identifier task.
            task - task for which a notification was created.
    """
    __tablename__ = 'notification'

    id = Column(Integer, primary_key=True)
    user = Column(String)
    date = Column(DateTime)
    shown = Column(Boolean)
    message = Column(String)

    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship("Task", back_populates="notifications")

    def __init__(self, user, date, message=None):
        self.user = user
        self.date = date
        self.shown = False
        self.message = message

    def __str__(self):
        result = "\nID: %d\n" % self.id
        result += "Task ID: %d\n" % self.task_id
        result += "Date: %s\n" % self.date.strftime("%d.%m.%Y %H:%M")

        if self.message is not None:
            result += "Message: '%s'\n" % self.message

        if self.shown:
            result += "Shown\n"

        return result
