"""
    This module contains a TaskPlan model that is used
    to automatically create tasks at specific time intervals.
"""

import datetime
from dateutil.relativedelta import relativedelta

from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    ForeignKey,
    Interval
)

from tr_library.database import Base


class TaskPlan(Base):
    """
        TaskPlan model:

        Attributes:
            id - task plan identifier.
            user - the creator of the task plan.
            start_time - time after which task creation begins.
            end_time - The time after which to stop creating tasks.
            interval - interval between the tasks being created.
            total_tasks - number of tasks to create.
            created_tasks - number of tasks created by the task plan.
            last_created - the time of the last task created.
            task_template_id - task template identifier.
            task_template - a template task from which a new task will be created.
            tasks - tasks created by the scheduler.
    """
    __tablename__ = 'task_plan'

    id = Column(Integer, primary_key=True)
    user = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    interval = Column(Interval)
    total_tasks = Column(Integer)
    created_tasks = Column(Integer)
    last_created = Column(DateTime)

    task_template_id = Column(Integer, ForeignKey('task_template.id'))
    task_template = relationship("TaskTemplate", back_populates="task_plan")

    tasks = relationship("Task", back_populates="task_plan")

    def __init__(
            self,
            user,
            interval,
            task_template,
            start_time=None,
            end_time=None,
            total_tasks=None):
        self.user = user
        self.interval = interval
        self.task_template = task_template
        self.total_tasks = total_tasks
        self.created_tasks = 0
        self.start_time = start_time if start_time is not None else datetime.datetime.now()
        self.end_time = end_time
        self.last_created = self.start_time - self.interval

    @property
    def status(self):
        if self.created_tasks == 0:
            return "TODO"
        if (self.end_time is not None and
                self._convert_to_relativedelta(self.interval) +
                self.last_created > self.end_time):
            return "DONE"
        if self.total_tasks is not None and self.created_tasks == self.total_tasks:
            return "DONE"

        return "IN_PROGRESS"

    @property
    def convert_interval_to_str(self):
        interval = self.interval
        if isinstance(interval, relativedelta):
            interval = str(interval).split("(")[1].replace(")", "")
        interval = interval.split("=+")
        interval.reverse()
        interval = " ".join(interval)

        return interval

    def _convert_to_relativedelta(self, interval):
        number = int(interval.split("=+")[1])
        str = interval.split("=+")[0]
        if str.lower() == "seconds":
            interval = relativedelta(seconds=number)
        elif str.lower() == "minutes":
            interval = relativedelta(minutes=number)
        elif str.lower() == "hours":
            interval = relativedelta(hours=number)
        elif str.lower() == "days":
            interval = relativedelta(days=number)
        elif str.lower() == "weeks":
            interval = relativedelta(weeks=number)
        elif str.lower() == "months":
            interval = relativedelta(months=number)
        elif str.lower() == "years":
            interval = relativedelta(years=number)

        return interval

    def __str__(self):
        result = "\nID: %d\n" % self.id
        result += "Task template ID: %d\n" % self.task_template_id

        interval = str(self.interval).split("(")[1].replace(")", "")
        result += "Interval: %s\n" % interval

        if self.start_time is not None:
            result += "Start time: %s\n" % self.start_time.strftime("%d.%m.%Y %H:%M")
        if self.end_time is not None:
            result += "End time: %s\n" % self.end_time.strftime("%d.%m.%Y %H:%M")
        if self.total_tasks is not None:
            result += "Total tasks: %s\n" % self.total_tasks
        result += "Created tasks: %s\n" % self.created_tasks
        result += "Last created: %s\n" % self.last_created.strftime("%d.%m.%Y %H:%M")

        tasks_id = ', '.join([str(task.id) for task in self.tasks])
        if len(tasks_id) >= 1:
            result += "Tasks: [%s]\n" % str(tasks_id)

        return result
