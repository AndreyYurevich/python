"""
    This package contains models that are used in the library.

    Modules:
        executor.py - describes a model for assigning rights to execute user tasks by other users.
        folder.py - describes a model for storing tasks.
        notification.py - describes the notification model.
        tag.py - describes a model for grouping tasks.
        task.py - describes the model of the task and the template.
        task_plan.py - describes the model of automatic creation of tasks after a certain time interval.
"""