"""
    This module contains the executor model that is used to grant
    the right to perform the task by other users and the association table
    for the relationship of a executor with a task.
"""

from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Table,
    Integer,
    String,
    ForeignKey
)

from tr_library.database import Base


task_executor_association = Table('task_executor', Base.metadata,
                                  Column('task_id', Integer, ForeignKey('task.id')),
                                  Column('executor_id', Integer, ForeignKey('executor.id'))
                                  )


class Executor(Base):
    """
        Executor model:

        Attributes:
            id - executor identifier.
            user - name user.
            tasks - tasks to which the user was granted access.
    """
    __tablename__ = 'executor'

    id = Column(Integer, primary_key=True)
    user = Column(String)

    tasks = relationship("Task",
                         secondary=task_executor_association,
                         back_populates="executors"
                         )

    def __init__(self, user):
        self.user = user

    def __str__(self):
        result = "\nID: %d, name: %s\n" % (self.id, self.user)
        tasks_id = ', '.join([str(task.id) for task in self.tasks])
        if len(tasks_id) >= 1:
            result += "Tasks ID: [%s]\n" % tasks_id

        return result
