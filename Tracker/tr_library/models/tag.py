"""
    This module contains a Tag model, which is used to grouping tasks
    and an association table for the relationship of a tag with a task.
"""

from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Table,
    Integer,
    String,
    ForeignKey
)

from tr_library.database import Base

task_tag_association = Table('task_tag', Base.metadata,
                             Column('task_id', Integer, ForeignKey('task.id')),
                             Column('tag_id', Integer, ForeignKey('tag.id'))
                             )


class Tag(Base):
    """
    Tag model:

    Attributes:
        id - tag identifier.
        user - the creator of the tag.
        name - tag name.
        tasks - tasks that contain this tag.
    """
    __tablename__ = 'tag'

    id = Column(Integer, primary_key=True)
    user = Column(String)
    name = Column(String)

    tasks = relationship("Task",
                         secondary=task_tag_association,
                         back_populates="tags"
                         )

    def __init__(self, user, name):
        self.user = user
        self.name = name

    def __str__(self):
        result = "\nID: %d, name: %s\n" % (self.id, self.name)
        tasks_id = ', '.join([str(task.id) for task in self.tasks])
        if len(tasks_id) >= 1:
            result += "Tasks ID: [%s]\n" % tasks_id

        return result
