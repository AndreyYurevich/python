"""
    This module contains the Folders model,
    which is used to storing tasks.
"""

from sqlalchemy.orm import (
    relationship,
    backref
)
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

from tr_library.database import Base


class Folder(Base):
    """
        Folder model:

        Attributes:
            id - folder identifier.
            user - the creator of the folder.
            name - folder name.
            parent_folder_id - identifier of the parent folder.
            subfolders - subfolders of this folder.
            tasks - tasks stored in a folder.
    """
    __tablename__ = 'folder'

    id = Column(Integer, primary_key=True)
    user = Column(String)
    name = Column(String)

    parent_folder_id = Column(Integer, ForeignKey('folder.id'))
    subfolders = relationship("Folder",
                              backref=backref('parent', remote_side="Folder.id")
                              )

    tasks = relationship("Task", back_populates="folder")

    def __init__(self, user, name):
        self.user = user
        self.name = name

    def __str__(self):
        result = "\nID: %d, name: %s\n" % (self.id, self.name)
        subfolders_id = ', '.join([str(folder.id) for folder in self.subfolders])
        if len(subfolders_id) >= 1:
            result += "Subfolders ID: [%s]\n" % subfolders_id

        return result
