"""
    This module contains the Task model and TaskTemplate,
    enumeration of priority and status.
"""

import enum
from datetime import datetime

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import (
    relationship,
    backref
)
from sqlalchemy import (
    Column,
    String,
    Integer,
    DateTime,
    Enum,
    ForeignKey
)

from tr_library.models.tag import task_tag_association
from tr_library.models.executor import task_executor_association
from tr_library.database import Base


class Priority(enum.Enum):
    """"Enum for storing task priority"""
    LOW = 1
    MEDIUM = 2
    HIGH = 3


class Status(enum.Enum):
    """"Enum for storing task status"""
    TODO = 1
    IN_PROGRESS = 2
    DONE = 3
    FAILED = 4


class Task(Base):
    """
    Task model:

    Attributes:
        id - task identifier.
        group - the task group (for subtasks - the root task id, for the root task - 0).
        user - the creator of the task.
        name - task name.
        start_time - the start time for the task.
        end_time - the end time for the task.
        priority - task priority.
        status - task execution status.
        parent_task_id - identifier of the parent task.
        subtasks - subtasks of this task.
        tag - task tags.
        task_plan_id - task plan identifier.
        task_plan - task plan that created the task.
        notifications - reminding about task.
        executors - users who can perform and complete a task.
        folder_id - folder identifier.
        folder - the folder in which the task tree is stored.
    """
    __tablename__ = 'task'

    id = Column(Integer, primary_key=True)
    group = Column(Integer)
    user = Column(String)
    name = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    priority = Column(Enum(Priority))
    _status = Column("status", Enum(Status))

    parent_task_id = Column(Integer, ForeignKey('task.id'))
    subtasks = relationship("Task",
                            backref=backref("parent", remote_side=[id])
                            )

    tags = relationship("Tag",
                        secondary=task_tag_association,
                        back_populates="tasks"
                        )

    task_plan_id = Column(Integer, ForeignKey('task_plan.id'))
    task_plan = relationship("TaskPlan", back_populates="tasks")

    notifications = relationship("Notification", back_populates="task")

    executors = relationship(
        "Executor",
        secondary=task_executor_association,
        back_populates="tasks"
    )

    folder_id = Column(Integer, ForeignKey('folder.id'))
    folder = relationship("Folder", back_populates="tasks")

    def __init__(
            self,
            user,
            name,
            start_time=None,
            end_time=None,
            priority=None,
            status=None):
        self.group = 0
        self.user = user
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.priority = priority if priority is not None else Priority.LOW
        self.status = status if status is not None else Status.TODO

    def __str__(self):
        result = "\nID: %d, name: %s\n" % (self.id, self.name)
        if self.parent_task_id is not None:
            result += "Parent task ID: %d\n" % self.parent_task_id
        result += "Start time: %s\n" % self.start_time.strftime("%d.%m.%Y %H:%M")
        if self.end_time is not None:
            result += "End time: %s\n" % self.end_time.strftime("%d.%m.%Y %H:%M")
        result += "Priority: %s\n" % self.priority.name
        result += "Status: %s\n" % self.status.name
        if self.folder is not None:
            result += "Folder: %s (ID: %d)\n" % (self.folder.name, self.folder.id)

        tags_name = ', '.join([tag.name for tag in self.tags])
        if len(tags_name) >= 1:
            result += "Tags: [%s]\n" % tags_name

        subtasks_id = ', '.join([str(task.id) for task in self.subtasks])
        if len(subtasks_id) >= 1:
            result += "Subtasks ID: [%s]\n" % subtasks_id

        executors = ', '.join([str(executor.user) for executor in self.executors])
        if len(executors) >= 1:
            result += "Executors: [%s]\n" % executors

        return result

    @hybrid_property
    def status(self):
        if self._status == Status.TODO and datetime.now() >= self.start_time:
            self._status = Status.IN_PROGRESS
        if self._status == Status.IN_PROGRESS and datetime.now() < self.start_time:
            self._status = Status.TODO
        if self.end_time is not None:
            if self._status == Status.IN_PROGRESS and datetime.now() >= self.end_time:
                self._status = Status.FAILED

        return self._status

    @status.setter
    def status(self, value):
        self._status = value


class TaskTemplate(Base):
    """
    Task model:

    Attributes:
        id - task identifier.
        user - the creator of the task.
        name - task name.
        start_time - the start time for the task.
        end_time - the end time for the task.
        priority - task priority.
        task_plan - task plan that created the task.
    """
    __tablename__ = 'task_template'

    id = Column(Integer, primary_key=True)
    user = Column(String)
    name = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    priority = Column(Enum(Priority))

    task_plan = relationship("TaskPlan",
                             back_populates="task_template",
                             uselist=False
                             )

    def __init__(
            self,
            user,
            name,
            start_time=None,
            end_time=None,
            priority=None):
        self.user = user
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.priority = priority

    def __repr__(self):
        return "<TaskTemplate ID: %s, name: %s>" % (self.id, self.name)
