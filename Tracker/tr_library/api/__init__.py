"""
    This package contains api for working with database models.

    Modules:
        api.py - describes the logic of interaction with the database model.
        exceptions.py - describes the exceptions that can occur while working with the database.
"""