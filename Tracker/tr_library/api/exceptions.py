"""
    This module contains exception classes.
"""


class LibraryError(Exception):
    """Main class of library exceptions."""


class TimeError(LibraryError):
    """Exclusion in incorrect work with time."""


class ObjectNotFoundError(LibraryError):
    """An exception indicating that there is no object in the database."""


class StatusError(LibraryError):
    """Exception about incorrect actions on an object with a certain status."""


class AttachError(LibraryError):
    """Exception during establishment of communication between objects."""


class DetachError(LibraryError):
    """Exception during breaking of communication between objects."""
