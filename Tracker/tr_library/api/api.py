"""
    This module contains the api class for interacting with the database
    and for obtaining all tr_library functions.
"""

import re
import datetime
from dateutil.relativedelta import relativedelta

import sqlalchemy
from sqlalchemy import (
    and_,
    or_
)

from tr_library.database import get_session
from tr_library.logger import log_decorator
from tr_library.models.folder import Folder
from tr_library.models.task_plan import TaskPlan
from tr_library.models.notification import Notification
from tr_library.models.tag import Tag
from tr_library.models.executor import Executor
from tr_library.models.task import (
    Task,
    Status,
    Priority,
    TaskTemplate
)
from tr_library.api.exceptions import (
    ObjectNotFoundError,
    TimeError,
    AttachError,
    DetachError,
    StatusError
)


class API:
    """Class of interaction with the database."""
    def __init__(self, connection_string):
        """Initializing the connection to the database."""
        self._session = get_session(connection_string)

    def __del__(self):
        """Removes database connection."""
        if hasattr(self, "_session") and self._session is not None:
            self._session.close()

    @log_decorator
    def get_task(self, user, task_id):
        """
            Returns the task by its identifier or throws
            an exception if the task is not found.
        """
        try:
            return (self._session.query(Task).filter(Task.user == user)
                    .filter(Task.id == task_id).one())
        except sqlalchemy.orm.exc.NoResultFound:
            try:
                task = (self._session.query(Task).join(Task.executors)
                        .filter(Executor.user == user))
                return task.filter(Task.id == task_id).one()
            except sqlalchemy.orm.exc.NoResultFound:
                raise ObjectNotFoundError("Task not found")

    @log_decorator
    def get_tasks(self,
                  user,
                  group=None,
                  name=None,
                  start_time=None,
                  end_time=None,
                  priority=None,
                  status=None,
                  parent_task_id=None,
                  task_plan_id=None,
                  folder_id=None,
                  without_folder=False,
                  parent_task=False,
                  executor=False):
        """Returns tasks that match the specified filter."""
        if priority is not None:
            try:
                priority = Priority[priority.upper()]
            except:
                raise KeyError("Incorrect priority")

        _filter = and_(
            or_(group is None, Task.group == group),
            or_(name is None, Task.name == name),
            or_(start_time is None, Task.start_time == start_time),
            or_(end_time is None, Task.end_time == end_time),
            or_(priority is None, Task.priority == priority),
            or_(parent_task_id is None, Task.parent_task_id == parent_task_id),
            or_(task_plan_id is None, Task.task_plan_id == task_plan_id),
            or_(folder_id is None, Task.folder_id == folder_id),
            or_(not without_folder, Task.folder_id == None),
            or_(not parent_task, Task.parent_task_id == None)
        )

        if executor:
            task = (self._session.query(Task).join(Task.executors)
                    .filter(Executor.user == user))
        else:
            task = self._session.query(Task).filter(Task.user == user)
        task = task.filter(_filter).all()

        if status is not None:
            tasks = task
            task = []
            for t in tasks:
                if t.status == status:
                    task.append(t)

        self._session.commit()

        return task

    @log_decorator
    def create_task(self,
                    user,
                    name,
                    start_time=None,
                    end_time=None,
                    priority=None):
        """
            Creates a task from the data set.

            :param user: task creator.
            :param name: task name.
            :param start_time: the start time for the task.
            :param end_time: the end time for the task.
            :param priority: task priority
            :return: identifier of the created task.
        """
        if start_time is None:
            start_time = datetime.datetime.now()

        if end_time is not None and end_time < start_time:
            raise TimeError("Start time is longer than end time")

        if start_time <= datetime.datetime.now():
            status = Status.IN_PROGRESS
        else:
            status = Status.TODO

        if priority is None:
            priority = Priority.LOW
        else:
            try:
                priority = Priority[priority.upper()]
            except:
                raise KeyError("Incorrect priority")

        task = Task(
            user=user,
            name=name,
            start_time=start_time,
            end_time=end_time,
            priority=priority,
            status=status
        )

        self._add(task)

        return task.id

    @log_decorator
    def edit_task(self,
                  user,
                  task_id,
                  name=None,
                  start_time=None,
                  end_time=None,
                  priority=None):
        """Edits the specified task fields."""
        task = self.get_task(user, task_id)

        if name is not None:
            task.name = name

        if start_time is not None:
            task.start_time = start_time

        if end_time is not None and end_time < task.start_time:
            raise TimeError("End time is longer than start time")
        elif end_time is not None and end_time < datetime.datetime.now():
            raise TimeError("The task end time is less than the current time")
        else:
            task.end_time = end_time

        if task.start_time >= datetime.datetime.now():
            task.status = Status.IN_PROGRESS
        else:
            task.status = Status.TODO

        if priority is not None:
            try:
                task.priority = Priority[priority.upper()]
            except:
                raise KeyError("Incorrect priority")

        self._session.commit()

    @log_decorator
    def attach_subtask(self, user, task_id, subtask_id):
        """
            Adds a subtask to a given task and moves the subtask
            and its tree to the folder to the task.
        """
        task = self.get_task(user, task_id)
        subtask = self.get_task(user, subtask_id)

        if subtask.parent_task_id is not None:
            raise AttachError("The subtask has a task")
        if task.id == subtask.id:
            raise AttachError("Task and subtask one object")
        if task.group == subtask.id:
            raise AttachError("The attachment forms a cycle")
        if task.group == subtask.group and task.group != 0:
            raise AttachError("The attachment forms a cycle")
        if subtask.end_time is not None and subtask.end_time < task.start_time:
            raise TimeError("The subtask ends before the task begins")
        if task.end_time is not None and task.end_time < subtask.start_time:
            raise TimeError("The task ends before the subtask begins")
        if task.status == Status.FAILED or subtask.status == Status.FAILED:
            raise StatusError("The task has the status failed")

        task.subtasks.append(subtask)
        group_id = task.group
        if task.group == 0:
            group_id = task.id

        subtask.executors = task.executors

        self._fix_group(subtask, group_id)

        self._fix_folder(user, subtask.id, task.folder)

        self._session.commit()

    @log_decorator
    def detach_subtask(self, user, subtask_id):
        """Removes a subtask from a given task."""
        subtask = self.get_task(user, subtask_id)

        if subtask.parent_task_id is None:
            raise DetachError("The task does not have a parent task")
        else:
            task = self.get_task(user, subtask.parent_task_id)
            task.subtasks.remove(subtask)

            self._fix_group(subtask, 0)

        self._session.commit()

    def _fix_group(self, task, group):
        """Fix group in task tree."""
        task.group = group
        if group == 0:
            group = task.id

        for subtask in task.subtasks:
            self._fix_group(subtask, group)

    def _fix_folder(self, user, task_id, folder):
        """Moves tasks to one folder."""
        task = self.get_task(user, task_id)

        task.folder = folder
        for subtask in task.subtasks:
            self._fix_folder(user, subtask.id, folder)

    @log_decorator
    def complete_task(self, user, task_id):
        """Completes the task and changes the status to done."""
        task = self._session.query(Task).filter_by(id=task_id).first()

        if task is None or not self._check_is_the_executor(user, task):
            raise ObjectNotFoundError("Task not found")
        if task.status != Status.IN_PROGRESS:
            raise StatusError("Task is not in progress")

        for notification in task.notifications:
            if not notification.shown:
                self.delete_notification(user, notification.id)

        self._complete_subtasks(user, task)

        self._session.commit()

    def _complete_subtasks(self, user, task):
        """Completes the subtask and changes the status to done."""
        if self._check_is_the_executor(user, task):
            task.status = Status.DONE

            for notification in task.notifications:
                if not notification.shown:
                    self.delete_notification(user, notification.id)

            for task in task.subtasks:
                self._complete_subtasks(user, task)

    def _check_is_the_executor(self, user, task):
        """Checks if the user has the right to complete the task."""
        if task.user == user:
            return True

        for executor in task.executors:
            if executor.user == user:
                return True

        return False

    @log_decorator
    def get_task_tree(self, user, task_id):
        """Returns the tasks tree."""
        task = self.get_task(user, task_id)
        task_tree = self._task_tree(task)

        return task_tree

    def _task_tree(self, task):
        """Forms a task tree."""
        task_tree = [task]

        for subtask in task.subtasks:
            subtask_tree = self._task_tree(subtask)
            task_tree.append(subtask_tree)

        return task_tree

    @log_decorator
    def delete_task(self, user, task_id, with_subtasks=True):
        """Deletes the task and its tree."""
        task = self.get_task(user, task_id)

        for subtask in task.subtasks:
            if with_subtasks:
                self.delete_task(user, subtask.id)
            else:
                self._fix_group(subtask, 0)

        for tag in task.tags:
            if len(tag.tasks) == 1:
                self._delete(tag)

        for executor in task.executors:
            if len(executor.tasks) == 1:
                self._delete(executor)

        for notification in task.notifications:
            self._delete(notification)

        self._delete(task)

    @log_decorator
    def get_folder(self, user, folder_id):
        """
            Returns the folder by its identifier or throws
            an exception if the folder is not found.
        """
        try:
            return (self._session.query(Folder)
                    .filter_by(user=user)
                    .filter_by(id=folder_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Folder not found")

    @log_decorator
    def get_folders(self, user, name=None, parent_folder_id=None):
        """Returns folders that match the specified filter."""
        _filter = and_(
            or_(name is None, Folder.name == name),
            or_(parent_folder_id is None,
                Folder.parent_folder_id == parent_folder_id)
        )

        folders = self._session.query(Folder).filter_by(user=user)

        return folders.filter(_filter).all()

    @log_decorator
    def create_folder(self, user, name, parent_folder_id=None):
        """
            Creates a folder from the data set.

            :param user: folder creator.
            :param name: folder name.
            :param parent_folder_id: folder id where a new folder should be created.
            :return: identifier of the created folder.
        """
        folder = Folder(user=user, name=name)

        if parent_folder_id is not None:
            parent_folder = self.get_folder(user, parent_folder_id)
            parent_folder.subfolders.append(folder)

        self._add(folder)

        return folder.id

    @log_decorator
    def edit_folder(self, user, folder_id, name=None):
        """Edits folder name."""
        folder = self.get_folder(user, folder_id)

        if name is not None:
            folder.name = name

        self._session.commit()

    @log_decorator
    def add_task_to_folder(self, user, task_id, folder_id):
        """Adds a task tree to a folder."""
        task = self.get_task(user, task_id)
        folder = self.get_folder(user, folder_id)

        if task.folder_id == folder_id:
            raise AttachError("The task is already in this folder")

        task.folder = folder

        if task.group == 0:
            self._fix_folder(user, task.id, folder)
        else:
            self._fix_folder(user, task.group, folder)

        self._session.commit()

    @log_decorator
    def delete_task_from_folder(self, user, task_id):
        """Deletes the task tree from the folder."""
        task = self.get_task(user, task_id)

        if task.folder is None:
            raise DetachError("The task does not have a folder")
        else:
            folder = task.folder

        folder.tasks.remove(task)

        if task.group == 0:
            self._fix_folder(user, task.id, None)
        else:
            self._fix_folder(user, task.group, None)

        self._session.commit()

    @log_decorator
    def get_folder_tree(self, user, folder_id):
        """Returns the folder tree."""
        folder = self.get_folder(user, folder_id)
        folder_tree = self._task_tree(folder)

        return folder_tree

    def _folder_tree(self, folder):
        """Forms a folder tree."""
        folder_tree = [folder]

        for subfolder in folder.subfolders:
            subfolder_tree = self._task_tree(subfolder)
            folder_tree.append(subfolder_tree)

        return folder_tree

    @log_decorator
    def delete_folder(self, user, folder_id, with_subfolders=False):
        """Deletes the folder and its tree."""
        folder = self.get_folder(user, folder_id)

        if with_subfolders:
            for subfolder in folder.subfolders:
                self.delete_folder(user, subfolder.id, with_subfolders=True)

        self._delete(folder)

    @log_decorator
    def get_tag(self, user, tag_id):
        """
            Returns the tag by its identifier or throws
            an exception if the tag is not found.
        """
        try:
            return (self._session.query(Tag)
                    .filter_by(user=user)
                    .filter_by(id=tag_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Tag not found")

    @log_decorator
    def get_tags(self, user, name=None, task_id=None):
        """Returns tags that match the specified filter."""
        tags = (self._session.query(Tag)
                .filter(Tag.user == user)
                .filter(or_(name is None, Tag.name == name))
                .join(Tag.tasks)
                .filter(or_(task_id is None, Task.id == task_id))
                .all())

        return tags

    @log_decorator
    def add_tag(self, user, task_id, name=None, tag_id=None):
        """
            Creates or added a tag from the data set.

            :param user: tag creator.
            :param task_id: task identifier to which the tag is add.
            :param name: tag name.
            :param tag_id: identifier for the tag being add.
            :return: identifier of the created or added tag.
        """
        task = self.get_task(user, task_id)

        if tag_id is not None:
            tag = self.get_tag(user, tag_id)
        elif name is not None:
            tag = (self._session.query(Tag)
                   .filter_by(user=user)
                   .filter_by(name=name)
                   .first())

        if tag in task.tags:
            raise AttachError("This tag is attached to the task")
        if tag is None:
            tag = Tag(user=user, name=name)

        task.tags.append(tag)

        self._add(tag)

        return tag.id

    @log_decorator
    def edit_tag(self, user, tag_id, name):
        """Edits tag name."""
        tag = self.get_tag(user, tag_id)
        tags = self.get_tags(user=user, name=name)

        if tags is None:
            tag.name = name

        self._session.commit()

    @log_decorator
    def delete_tag(self, user, tag_id, task_id=None):
        """Deletes the tag completely or only for a specific task."""
        tag = self.get_tag(user, tag_id)

        if task_id is None:
            self._delete(tag)
        else:
            task = self.get_task(user, task_id)
            tag.tasks.remove(task)

            self._session.commit()

    @log_decorator
    def get_notification(self, user, notification_id):
        """
            Returns the notification by its identifier or throws
            an exception if the task is not found.
        """
        try:
            return (self._session.query(Notification)
                    .filter_by(user=user)
                    .filter_by(id=notification_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Notification not found")

    @log_decorator
    def get_notifications(self,
                          user,
                          date=None,
                          shown=None,
                          message=None,
                          task_id=None):
        """Returns notifications that match the specified filter."""
        _filter = and_(
            Notification.user == user,
            or_(date is None, Notification.date == date),
            or_(shown is None, Notification.shown == shown),
            or_(message is None, Notification.message == message),
            or_(task_id is None, Notification.task_id == task_id)
        )

        notifications = self._session.query(Notification)

        return notifications.filter(_filter).all()

    @log_decorator
    def add_notification(self, user, date, task_id, message=None):
        """
            Creates a notification from the data set.

            :param user: notification creator.
            :param date: time of notification show.
            :param task_id: task identifier to which the notification is add.
            :param message: notification message.
            :return: identifier of the created notification.
        """
        task = self.get_task(user, task_id)

        if date < task.start_time or date < datetime.datetime.now():
            raise TimeError("Incorrect notification date")

        if task.end_time is not None and date > task.end_time:
            raise TimeError("Incorrect notification date")

        notification = Notification(user=user, date=date, message=message)
        task.notifications.append(notification)

        self._add(notification)

        return notification.id

    @log_decorator
    def edit_notification(self, user, notification_id, date=None, message=None):
        """Edits the specified notification fields."""
        notification = self.get_notification(user, notification_id)

        if date is not None and date > datetime.datetime.now():
            notification.date = date

        if message is not None:
            notification.message = message

        self._session.commit()

    @log_decorator
    def show_notifications(self, user):
        """Shows all missed notifications."""
        notifications = (self._session.query(Notification)
                         .filter_by(user=user)
                         .filter_by(shown=False)
                         .filter(Notification.date <= datetime.datetime.now())
                         .all())

        for notification in notifications:
            notification.shown = True

        self._session.commit()

        return notifications

    @log_decorator
    def delete_notification(self, user, notification_id):
        """Deletes the notification by id."""
        notification = self.get_notification(user, notification_id)

        self._delete(notification)

    @log_decorator
    def get_task_plan(self, user, task_plan_id):
        """
            Returns the task plan by its identifier or throws
            an exception if the task plan is not found.
        """
        try:
            task_plan = (self._session.query(TaskPlan)
                         .filter_by(user=user)
                         .filter_by(id=task_plan_id)
                         .one())
            task_plan.interval = relativedelta() + task_plan.interval

            return task_plan
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("TaskPlan not found")

    @log_decorator
    def get_task_plans(self,
                       user,
                       interval=None,
                       total_tasks=None,
                       created_tasks=None,
                       last_created=None):
        """Returns task plans that match the specified filter."""
        if interval is not None:
            number = int(interval.split(" ")[0])
            str = interval.split(" ")[1]
            if str.lower() in ["s", "sec", "second"]:
                interval = relativedelta(seconds=number)
            elif str.lower() in ["m", "min", "minute"]:
                interval = relativedelta(minutes=number)
            elif str.lower() in ["h", "hour"]:
                interval = relativedelta(hours=number)
            elif str.lower() in ["d", "day"]:
                interval = relativedelta(days=number)
            elif str.lower() in ["w", "week"]:
                interval = relativedelta(weeks=number)
            elif str.lower() in ["mon", "month"]:
                interval = relativedelta(months=number)
            elif str.lower() in ["y", "year"]:
                interval = relativedelta(years=number)
            else:
                raise TimeError("Invalid interval")

        _filter = and_(
            TaskPlan.user == user,
            or_(interval is None, TaskPlan.interval == interval),
            or_(total_tasks is None, TaskPlan.total_tasks == total_tasks),
            or_(created_tasks is None, TaskPlan.created_tasks == created_tasks),
            or_(last_created is None, TaskPlan. last_created == last_created)
        )

        task_plans = self._session.query(TaskPlan).filter(_filter).all()

        for task_plan in task_plans:
            task_plan.interval = relativedelta() + task_plan.interval

        return task_plans

    @log_decorator
    def create_task_plan(self,
                         user,
                         interval,
                         task_id,
                         start_time=None,
                         end_time=None,
                         total_tasks_count=None):
        """
            Creates a task plan from the data set.

            :param user: task plan creator.
            :param interval: interval between the tasks being created.
            :param task_id: task id from which the template task will be created.
            :param start_time: time after which task creation begins.
            :param end_time: The time after which to stop creating tasks.
            :param total_tasks_count: number of tasks to create.
            :return: identifier of the created task plan.
        """
        if end_time is not None and end_time < start_time:
            raise TimeError("Start time is longer than end time")
        if start_time is not None and start_time < datetime.datetime.now():
            raise TimeError("Start time is longer than the current time")

        task = self.get_task(user, task_id)
        task_template_id = self.create_task_template(user, task.id)
        task_template = self.get_task_template(user, task_template_id)

        try:
            number = int(interval.split(" ")[0])
            str = interval.split(" ")[1]
        except IndexError:
            raise TimeError("Invalid interval")

        if str.lower() in ["s", "sec", "second"]:
            interval = relativedelta(seconds=number)
        elif str.lower() in ["m", "min", "minute"]:
            interval = relativedelta(minutes=number)
        elif str.lower() in ["h", "hour"]:
            interval = relativedelta(hours=number)
        elif str.lower() in ["d", "day"]:
            interval = relativedelta(days=number)
        elif str.lower() in ["w", "week"]:
            interval = relativedelta(weeks=number)
        elif str.lower() in ["mon", "month"]:
            interval = relativedelta(months=number)
        elif str.lower() in ["y", "year"]:
            interval = relativedelta(years=number)
        else:
            raise TimeError("Invalid interval")

        plan = TaskPlan(
            user=user,
            interval=interval,
            task_template=task_template,
            start_time=start_time,
            end_time=end_time,
            total_tasks=total_tasks_count
        )

        self._add(plan)

        return plan.id

    @log_decorator
    def generate_a_task_plan(self, user):
        """Creates task plans tasks."""
        plans = self.get_task_plans(user)

        for plan in plans:
            while self._check_can_create_a_task_plan(plan):
                task_template = plan.task_template
                start_time = plan.last_created + plan.interval
                end_time = task_template.end_time

                if end_time is not None:
                    interval = relativedelta(
                        end_time,
                        task_template.start_time
                    )
                    end_time = start_time + interval

                task = Task(
                    user=task_template.user,
                    name=task_template.name,
                    start_time=start_time,
                    end_time=end_time,
                    priority=task_template.priority,
                    status=Status.IN_PROGRESS
                )

                plan.tasks.append(task)
                plan.created_tasks += 1
                plan.last_created += plan.interval

                self._add(task)

        self._session.commit()

    def _check_can_create_a_task_plan(self, plan):
        """Checks if you can create a new scheduler task."""
        next_create_time = plan.last_created + plan.interval

        if plan.end_time is not None and plan.end_time < next_create_time:
            return False

        if (next_create_time <= datetime.datetime.now() and
                (plan.total_tasks is None or
                 plan.total_tasks >= plan.created_tasks + 1)):
            return True

        return False

    @log_decorator
    def delete_task_plan(self, user, task_plan_id):
        """Deletes the task plan for id."""
        task_plan = self.get_task_plan(user, task_plan_id)
        task_template = task_plan.task_template

        self._delete(task_plan)
        self._delete(task_template)

    @log_decorator
    def get_task_template(self, user, task_template_id):
        """
            Returns the task tamplate by its identifier or throws
            an exception if the task template is not found.
        """

        try:
            return (self._session.query(TaskTemplate)
                    .filter_by(user=user)
                    .filter_by(id=task_template_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("TaskTemplate not found")

    @log_decorator
    def get_task_templates(self,
                           user,
                           name=None,
                           start_time=None,
                           end_time=None,
                           priority=None,
                           task_plan_id=None):
        """Returns task templates that match the specified filter."""
        _filter = and_(
            TaskTemplate.user == user,
            or_(name is None, Task.name == name),
            or_(start_time is None, Task.start_time == start_time),
            or_(end_time is None, Task.end_time == end_time),
            or_(priority is None, Task.priority == priority),
            or_(task_plan_id is None, Task.task_plan_id == task_plan_id)
        )

        task_templates = (self._session.query(TaskTemplate)
                          .filter(_filter).all())
        return task_templates

    @log_decorator
    def create_task_template(self, user, task_id):
        """
            Creates a task plan from the data set.

            :param user: task template creator.
            :param task_id: the task identifier for creating the task template.
            :return: identifier of the created task template.
        """
        task = self.get_task(user, task_id)

        task_template = TaskTemplate(
            user=task.user,
            name=task.name,
            start_time=task.start_time,
            end_time=task.end_time,
            priority=task.priority
        )

        self._add(task_template)

        return task_template.id

    @log_decorator
    def delete_task_template(self, user, task_template_id):
        """Deletes the task template for id."""
        task_template = self.get_task_template(user, task_template_id)

        self._delete(task_template)

    @log_decorator
    def get_executor(self, executor_id):
        """
            Returns the executor by its identifier or throws
            an exception if the executor is not found.
        """
        try:
            return (self._session.query(Executor)
                    .filter_by(id=executor_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Executor not found")

    @log_decorator
    def get_executors(self, user, task_id=None, executor_user=None):
        """Returns executors that match the specified filter."""
        executors = (self._session.query(Executor)
                     .filter(or_(executor_user is None,
                                 Executor.user == executor_user))
                     .join(Executor.tasks)
                     .filter(or_(task_id is None, Task.id == task_id)))

        return executors

    @log_decorator
    def add_executor_to_task(self,
                             user,
                             task_id,
                             executor_user,
                             to_subtasks=False):
        """
            Creates or added a executor from the data set.

            :param user: the name of the user for executor the task is added.
            :param task_id: task identifier to which the executor is add.
            :param executor_user: executor name.
            :param to_subtasks: add to subtasks.
            :return: identifier of the created or added executor.
        """
        if user == executor_user:
            raise AttachError("The performer is the user of the task")

        task = self.get_task(user, task_id)
        executor = (self._session.query(Executor)
                    .filter_by(user=executor_user)
                    .first())

        if executor is not None and executor in task.executors:
            raise AttachError("The user is already the task executor")
        elif executor is None:
            executor = Executor(user=executor_user)

        task.executors.append(executor)
        if to_subtasks:
            self._add_executor_to_subtask(task, executor)

        self._session.commit()

        return executor.id

    def _add_executor_to_subtask(self, task, executor):
        for subtask in task.subtasks:
            if executor not in subtask.executors:
                subtask.executors.append(executor)

            self._add_executor_to_subtask(subtask, executor)

    @log_decorator
    def delete_executor_from_task(self,
                                  user,
                                  task_id,
                                  executor_user,
                                  to_subtasks=False):
        """Delete executor from task."""
        if user == executor_user:
            raise DetachError("The performer is the user of the task")

        task = self.get_task(user, task_id)
        executor = (self._session.query(Executor)
                    .filter_by(user=executor_user)
                    .first())

        if executor is None:
            raise DetachError("User is not a executor")
        if executor not in task.executors:
            raise DetachError("User is not the executor of the task")
        else:
            task.executors.remove(executor)
            if to_subtasks:
                self._delete_executor_from_subtask(task, executor)

        self._session.commit()

    def _delete_executor_from_subtask(self, task, executor):
        """Delete executor from subtask."""
        for subtask in task.subtasks:
            if executor in subtask.executors:
                subtask.executors.remove(executor)
            self._delete_executor_from_subtask(subtask, executor)

    def search_task(
            self,
            user,
            name=None,
            start_time=None,
            end_time=None,
            priority=None,
            status=None,
            parent_task_id=None,
            executor=None
    ):
        """Searches for tasks that match the specified filter."""
        tasks = self.get_tasks(
            user,
            start_time=start_time,
            end_time=end_time,
            priority=priority,
            status=status,
            parent_task_id=parent_task_id,
            executor=executor
        )

        if name:
            tasks = [task for task in tasks if re.search(name, task.name)]

        return tasks

    def _delete(self, object):
        """Deletes object from database."""
        self._session.delete(object)
        self._session.commit()

    def _add(self, object):
        """Add object to database."""
        self._session.add(object)
        self._session.commit()
