#Tracker

A simple application for managing tasks. Allows you to create and edit your tasks, set due dates, track progress, set tags and folders to group tasks, create notifications to remind you of task unfinished, create plan to automatically create tasks by template. In addition, you can grant the right to execute tasks to other users.

## Installing:

### Cloning:

```bash
$ git clone https://AndreyYurevich@bitbucket.org/AndreyYurevich/python.git
```

### Installing:

```bash
$ cd python/Tracker
$ python3 setup.py install
```

### Running tests:

```bash
$ python3 setup.py test
```



## Work with users

You can view and change current user 

```bash
$ tracker user [command]
```

Change current user:

```bash
$ tracker user change username
```

View current user:

```bash
$ tracker user show
```



## Work with tasks

```bash
$ tracker task [command][params]
```

Add new task:

```bash
$ tracker task add [name]
```

Edit task:

```bash
$ tracker task edit [task_id]
```

Attach task to another task:

```bash
$ tracker task attach [task_id][subtask_id]
```

Detach subtask from task:

```bash
$ tracker task detach [subtask_id]
```

Show task detail:

```bash
$ tracker task show [task_id]
```

Delete task:

```bash
$ tracker task del [task_id]
```



## Work with task plans

Tasks can be created automatically. To do this, you need to create a task plan

```bash
$ tracker task_plan [command][params]
```

Create new task plan:

```bash
$ tracker task_plan add [interval][task_id]
```

Show task plan detail:

```bash
$ tracker task_plan show [task_plan_id]
```

Delete task plan:

```bash
$ tracker task_plan del [task_plan_id]
```



## Work with folders

You can group tasks into folders

```bash
$ tracker folder [command][params]
```

Create new folder:

```bash
$ tracker folder add [name]
```

Edit folder:

```bash
$ tracker folder edit [folder_id]
```

Show folder detail:

```bash
$ tracker folder show [folder_id]
```

Delete folder:

```bash
$ tracker folder del [folder_id]
```



## Work with notifications

You can create reminders for tasks

```bash
$ tracker notification [command][params]
```

Create new notification:

```bash
$ tracker notification add [task_id][date]
```

Edit notification:

```bash
$ tracker notification edit [notification_id]
```

Show notification detail:

```bash
$ tracker notification show [notification_id]
```

Delete notification:

```bash
$ tracker notification del [notification_id]
```



## Work with tags

```bash
$ tracker tag [command][params]
```

Create new tag:

```bash
$ tracker tag add [task_id][tag_name]
```

Edit tag:

```bash
$ tracker tag edit [task_id][new_name]
```

Show tag detail:

```bash
$ tracker tag show [tag_id]
```

Remove the tag both from the task and delete it completely:

```bash
$ tracker tag del [tag_id]
```



## Work with executors

You can grant the execution rights to other users:

```bash
$ tracker executor [command][params]
```

Add executor to task and subtask:

```bash
$ tracker executor add [task_id][username]
```

Show executor detail:

```bash
$ tracker executor show [executor_id]
```

Delete executor from task:

```bash
$ tracker executor del [task_id][executor_name]
```



## Running web version:

```bash
$ python3 manage.py runserver
```

